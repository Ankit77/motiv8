-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 27, 2016 at 02:31 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `motiv8`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE `assigned_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `challenges`
--

CREATE TABLE `challenges` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `craeted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_28_171741_create_social_logins_table', 1),
('2015_12_29_015055_setup_access_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `group_id`, `name`, `display_name`, `system`, `sort`, `created_at`, `updated_at`) VALUES
(1, 1, 'view-backend', 'View Backend', 1, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(2, 1, 'view-access-management', 'View Access Management', 1, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(3, 2, 'create-users', 'Create Users', 1, 5, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(4, 2, 'edit-users', 'Edit Users', 1, 6, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(5, 2, 'delete-users', 'Delete Users', 1, 7, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(6, 2, 'change-user-password', 'Change User Password', 1, 8, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(7, 2, 'deactivate-users', 'Deactivate Users', 1, 9, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(8, 2, 'reactivate-users', 'Re-Activate Users', 1, 11, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(9, 2, 'undelete-users', 'Restore Users', 1, 13, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(10, 2, 'permanently-delete-users', 'Permanently Delete Users', 1, 14, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(11, 2, 'resend-user-confirmation-email', 'Resend Confirmation E-mail', 1, 15, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(12, 3, 'create-roles', 'Create Roles', 1, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(13, 3, 'edit-roles', 'Edit Roles', 1, 3, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(14, 3, 'delete-roles', 'Delete Roles', 1, 4, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(15, 4, 'create-permission-groups', 'Create Permission Groups', 1, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(16, 4, 'edit-permission-groups', 'Edit Permission Groups', 1, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(17, 4, 'delete-permission-groups', 'Delete Permission Groups', 1, 3, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(18, 4, 'sort-permission-groups', 'Sort Permission Groups', 1, 4, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(19, 4, 'create-permissions', 'Create Permissions', 1, 5, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(20, 4, 'edit-permissions', 'Edit Permissions', 1, 6, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(21, 4, 'delete-permissions', 'Delete Permissions', 1, 7, '2016-06-14 06:51:26', '2016-06-14 06:51:26');

-- --------------------------------------------------------

--
-- Table structure for table `permission_dependencies`
--

CREATE TABLE `permission_dependencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `dependency_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_dependencies`
--

INSERT INTO `permission_dependencies` (`id`, `permission_id`, `dependency_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(2, 3, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(3, 3, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(4, 4, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(5, 4, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(6, 5, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(7, 5, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(8, 6, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(9, 6, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(10, 7, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(11, 7, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(12, 8, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(13, 8, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(14, 9, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(15, 9, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(16, 10, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(17, 10, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(18, 11, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(19, 11, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(20, 12, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(21, 12, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(22, 13, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(23, 13, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(24, 14, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(25, 14, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(26, 15, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(27, 15, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(28, 16, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(29, 16, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(30, 17, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(31, 17, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(32, 18, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(33, 18, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(34, 19, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(35, 19, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(36, 20, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(37, 20, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(38, 21, 1, '2016-06-14 06:51:26', '2016-06-14 06:51:26'),
(39, 21, 2, '2016-06-14 06:51:26', '2016-06-14 06:51:26');

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_groups`
--

INSERT INTO `permission_groups` (`id`, `parent_id`, `name`, `sort`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Access', 1, '2016-06-14 06:51:25', '2016-06-14 06:51:25'),
(2, 1, 'User', 1, '2016-06-14 06:51:25', '2016-06-14 06:51:25'),
(3, 1, 'Role', 2, '2016-06-14 06:51:25', '2016-06-14 06:51:25'),
(4, 1, 'Permission', 3, '2016-06-14 06:51:25', '2016-06-14 06:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2016-06-14 06:51:25', '2016-06-14 06:51:25'),
(2, 'User', 0, 2, '2016-06-14 06:51:25', '2016-06-14 06:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `set_theme` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `profile_pic`, `phone`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `set_theme`) VALUES
(1, 'Admin', 'info@webdevelopmentpark.com', '$2y$10$zBNyVVMHH3k5pTQgSXdtFOxeii331WxborSa8zjleZGToYyztE2Li', '', '', 1, 'b6d183188ecefa0a331853d6006fa283', 1, 'lNjoxGHgWNxMjozLYcxv6H7Ie5U1VBeU3SKvO44eXSFOeW9JGX3hjOvLVenE', '2016-06-14 06:51:24', '2016-06-14 07:03:55', NULL, ''),
(2, 'Anil Jain', 'aniljmk@gmail.com', '$2y$10$mGC3Q5UrGaxmT4/YYms0/O84vn9nRQFWBvQgz8w.HH/w95YWmk9qm', '', '', 1, '13144e25e1801e18ffbd97b3737f509a', 1, 'RcfE2Xm05UHReDQ27TMNDG7doeU0w1DglChvDue5H1zEIwihPxco7HK0AA3a', '2016-06-14 06:51:24', '2016-06-14 07:05:16', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_challenges`
--

CREATE TABLE `user_challenges` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `challenge_name` varchar(255) NOT NULL,
  `challenge_timeline` varchar(255) NOT NULL,
  `post_frequency` varchar(255) NOT NULL,
  `completion_reward` varchar(255) NOT NULL,
  `invite_friend` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_roles_user_id_foreign` (`user_id`),
  ADD KEY `assigned_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `challenges`
--
ALTER TABLE `challenges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_dependencies_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_dependencies_dependency_id_foreign` (`dependency_id`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_challenges`
--
ALTER TABLE `user_challenges`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `challenges`
--
ALTER TABLE `challenges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_challenges`
--
ALTER TABLE `user_challenges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  ADD CONSTRAINT `permission_dependencies_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_dependencies_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
