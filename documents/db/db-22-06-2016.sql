-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table motiv8.assigned_roles
DROP TABLE IF EXISTS `assigned_roles`;
CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_roles_user_id_foreign` (`user_id`),
  KEY `assigned_roles_role_id_foreign` (`role_id`),
  CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.assigned_roles: ~2 rows (approximately)
DELETE FROM `assigned_roles`;
/*!40000 ALTER TABLE `assigned_roles` DISABLE KEYS */;
INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
	(1, 1, 1),
	(2, 2, 2);
/*!40000 ALTER TABLE `assigned_roles` ENABLE KEYS */;


-- Dumping structure for table motiv8.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.migrations: ~4 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2015_12_28_171741_create_social_logins_table', 1),
	('2015_12_29_015055_setup_access_tables', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table motiv8.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


-- Dumping structure for table motiv8.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.permissions: ~21 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `group_id`, `name`, `display_name`, `system`, `sort`, `created_at`, `updated_at`) VALUES
	(1, 1, 'view-backend', 'View Backend', 1, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(2, 1, 'view-access-management', 'View Access Management', 1, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(3, 2, 'create-users', 'Create Users', 1, 5, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(4, 2, 'edit-users', 'Edit Users', 1, 6, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(5, 2, 'delete-users', 'Delete Users', 1, 7, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(6, 2, 'change-user-password', 'Change User Password', 1, 8, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(7, 2, 'deactivate-users', 'Deactivate Users', 1, 9, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(8, 2, 'reactivate-users', 'Re-Activate Users', 1, 11, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(9, 2, 'undelete-users', 'Restore Users', 1, 13, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(10, 2, 'permanently-delete-users', 'Permanently Delete Users', 1, 14, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(11, 2, 'resend-user-confirmation-email', 'Resend Confirmation E-mail', 1, 15, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(12, 3, 'create-roles', 'Create Roles', 1, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(13, 3, 'edit-roles', 'Edit Roles', 1, 3, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(14, 3, 'delete-roles', 'Delete Roles', 1, 4, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(15, 4, 'create-permission-groups', 'Create Permission Groups', 1, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(16, 4, 'edit-permission-groups', 'Edit Permission Groups', 1, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(17, 4, 'delete-permission-groups', 'Delete Permission Groups', 1, 3, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(18, 4, 'sort-permission-groups', 'Sort Permission Groups', 1, 4, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(19, 4, 'create-permissions', 'Create Permissions', 1, 5, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(20, 4, 'edit-permissions', 'Edit Permissions', 1, 6, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(21, 4, 'delete-permissions', 'Delete Permissions', 1, 7, '2016-06-14 12:21:26', '2016-06-14 12:21:26');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;


-- Dumping structure for table motiv8.permission_dependencies
DROP TABLE IF EXISTS `permission_dependencies`;
CREATE TABLE IF NOT EXISTS `permission_dependencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `dependency_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_dependencies_permission_id_foreign` (`permission_id`),
  KEY `permission_dependencies_dependency_id_foreign` (`dependency_id`),
  CONSTRAINT `permission_dependencies_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_dependencies_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.permission_dependencies: ~39 rows (approximately)
DELETE FROM `permission_dependencies`;
/*!40000 ALTER TABLE `permission_dependencies` DISABLE KEYS */;
INSERT INTO `permission_dependencies` (`id`, `permission_id`, `dependency_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(2, 3, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(3, 3, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(4, 4, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(5, 4, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(6, 5, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(7, 5, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(8, 6, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(9, 6, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(10, 7, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(11, 7, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(12, 8, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(13, 8, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(14, 9, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(15, 9, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(16, 10, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(17, 10, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(18, 11, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(19, 11, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(20, 12, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(21, 12, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(22, 13, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(23, 13, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(24, 14, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(25, 14, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(26, 15, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(27, 15, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(28, 16, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(29, 16, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(30, 17, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(31, 17, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(32, 18, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(33, 18, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(34, 19, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(35, 19, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(36, 20, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(37, 20, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(38, 21, 1, '2016-06-14 12:21:26', '2016-06-14 12:21:26'),
	(39, 21, 2, '2016-06-14 12:21:26', '2016-06-14 12:21:26');
/*!40000 ALTER TABLE `permission_dependencies` ENABLE KEYS */;


-- Dumping structure for table motiv8.permission_groups
DROP TABLE IF EXISTS `permission_groups`;
CREATE TABLE IF NOT EXISTS `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.permission_groups: ~4 rows (approximately)
DELETE FROM `permission_groups`;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
INSERT INTO `permission_groups` (`id`, `parent_id`, `name`, `sort`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Access', 1, '2016-06-14 12:21:25', '2016-06-14 12:21:25'),
	(2, 1, 'User', 1, '2016-06-14 12:21:25', '2016-06-14 12:21:25'),
	(3, 1, 'Role', 2, '2016-06-14 12:21:25', '2016-06-14 12:21:25'),
	(4, 1, 'Permission', 3, '2016-06-14 12:21:25', '2016-06-14 12:21:25');
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;


-- Dumping structure for table motiv8.permission_role
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.permission_role: ~0 rows (approximately)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;


-- Dumping structure for table motiv8.permission_user
DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE IF NOT EXISTS `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  KEY `permission_user_user_id_foreign` (`user_id`),
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.permission_user: ~0 rows (approximately)
DELETE FROM `permission_user`;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;


-- Dumping structure for table motiv8.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.roles: ~0 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', 1, 1, '2016-06-14 12:21:25', '2016-06-14 12:21:25'),
	(2, 'User', 0, 2, '2016-06-14 12:21:25', '2016-06-14 12:21:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table motiv8.social_logins
DROP TABLE IF EXISTS `social_logins`;
CREATE TABLE IF NOT EXISTS `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_foreign` (`user_id`),
  CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.social_logins: ~0 rows (approximately)
DELETE FROM `social_logins`;
/*!40000 ALTER TABLE `social_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_logins` ENABLE KEYS */;


-- Dumping structure for table motiv8.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table motiv8.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Admin', 'info@webdevelopmentpark.com', '$2y$10$zBNyVVMHH3k5pTQgSXdtFOxeii331WxborSa8zjleZGToYyztE2Li', 1, 'b6d183188ecefa0a331853d6006fa283', 1, 'lNjoxGHgWNxMjozLYcxv6H7Ie5U1VBeU3SKvO44eXSFOeW9JGX3hjOvLVenE', '2016-06-14 12:21:24', '2016-06-14 12:33:55', NULL),
	(2, 'Anil Jain', 'aniljmk@gmail.com', '$2y$10$mGC3Q5UrGaxmT4/YYms0/O84vn9nRQFWBvQgz8w.HH/w95YWmk9qm', 1, '13144e25e1801e18ffbd97b3737f509a', 1, 'RcfE2Xm05UHReDQ27TMNDG7doeU0w1DglChvDue5H1zEIwihPxco7HK0AA3a', '2016-06-14 12:21:24', '2016-06-14 12:35:16', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
