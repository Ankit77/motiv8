<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use DB, Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }
    public function themes()
    {	
    	$themes=DB::table("themes")->get();
    	return view('backend.pages.themes', compact('themes'));
    }
    public function addtheme()
    {
    	return view('backend.pages.addtheme');
    }
    public function savetheme()
    {   $request= Request::all();
    
            if($_REQUEST['theme_attribute']=="background")
            {        $image="";
                if(isset($request['theme_pic']))
                  {     
                        $file = $request['theme_pic'];
                      
                            $image = date_default_timezone_set('UTC').time().$file->getClientOriginalName();
                            $imgtitle=$file->getClientOriginalName();

                            $newFileName = substr($imgtitle, 0 , (strrpos($imgtitle, ".")));
                            //echo $newFileName;    die;    
                            $fmove = $file->move(public_path() . '/images/background/',  $image);  
                                
                     } 

                  $data = array(
                                'title'             =>$_REQUEST['title'], 
                                'status'            => 1,
                                'theme_attribute'   =>$_REQUEST['theme_attribute'],
                                "theme_color"       =>$image
                                ); 
                DB::table('themes')->insert($data);
                return redirect()->back()->withFlashSuccess('New theme created');
             }
             else
             {
                    $data = array(
                                'title'             =>$_REQUEST['title'], 
                                'status'            => 1,
                                'theme_attribute'   =>$_REQUEST['theme_attribute'],
                                "theme_color"       =>"#".$_REQUEST['theme_color']

                                ); 
                DB::table('themes')->insert($data);
                return redirect()->back()->withFlashSuccess('New theme created');
             }

    }
    public function deletetheme($id=NULL)
    { 
    	if(isset($id)){
    		DB::table('themes')->where('id',$id)->delete();
    		return redirect()->back()->withFlashSuccess('Theme successfully deleted');
    	}else{
    	return redirect()->back();}
    }
    public function deactivetheme($id=NULL)
    {
    	if(isset($id)){
    		DB::table('themes')->where('id',$id)->update(['status'=>0]);
    		return redirect()->back()->withFlashSuccess('Theme successfully deactivated');
    	}else{
    		return redirect()->back();
    	}

    }
     public function activetheme($id=NULL)
    {
    	if(isset($id)){
    		DB::table('themes')->where('id',$id)->update(['status'=>1]);
    		return redirect()->back()->withFlashSuccess('Theme successfully activated');
    	}else{return redirect()->back();}
    }
    public function viewuser($id=NULL)
    {
        $userdetail=DB::table('users')->where('id', $id)->get();
        return view('backend.pages.viewuser', compact('userdetail'));
    }
  public function edittheme($id=NULL)
  {
        $themes=DB::table('themes')->where('id',$id)->get();
    return view('backend.pages.edittheme', compact('themes'));
  }
   public function editthemesave()
  {     

        $request= Request::all();
    
            if($_REQUEST['theme_attribute']=="background")
            {        $image="";
                if(isset($request['theme_pic']))
                  { 
                        $file = $request['theme_pic'];
                        
                        $image = date_default_timezone_set('UTC').time().$file->getClientOriginalName();
                        $imgtitle=$file->getClientOriginalName();

                        $newFileName = substr($imgtitle, 0 , (strrpos($imgtitle, ".")));
                        //echo $newFileName;    die;    
                        $fmove = $file->move(public_path() . '/images/background/',  $image);  
                       
                                
                     } 

                  $data = array(
                                'title'             =>$_REQUEST['title'], 
                                'status'            => 1,
                                'theme_attribute'   =>$_REQUEST['theme_attribute'],
                                "theme_color"       =>$image
                                ); 
                DB::table('themes')->where(['id'=>$_REQUEST['id']])->update($data);
                return redirect()->back()->withFlashSuccess('Theme updated');
             }
             else
             {
                    $data = array(
                                'title'             =>$_REQUEST['title'], 
                                'status'            => 1,
                                'theme_attribute'   =>$_REQUEST['theme_attribute'],
                                "theme_color"       =>'#'.$_REQUEST['theme_color']

                                ); 
                DB::table('themes')->where(['id'=>$_REQUEST['id']])->update($data);
                return redirect()->back()->withFlashSuccess('Theme updated');
             }
    return redirect()->back();
  }
  public function viewtheme($id=NULL)
  {
     $themes=DB::table('themes')->where('id',$id)->get();
  
       return view('backend.pages.viewtheme', compact('themes'));
  }
  public function challenges()
  {
    $challenges=DB::table('challenges')->join('user_challenges', 'challenges.id','=', 'user_challenges.challenge_id')->select('challenges.*', 'user_challenges.challenge_timeline', 'user_challenges.post_frequency', 'user_challenges.completion_reward', 'user_challenges.invite_friend')->get();
    return view('backend.pages.challenges', compact('challenges') );
  }
  public function addchallenges()
  {
    
    return view('backend.pages.addchallenges');
  }
  public function savechallenges()
  {  
   


    $challenge=array(
                        'name'          =>$_REQUEST['name'],
                        'description'   =>$_REQUEST['description'],
                        'status'        =>1 );
    $challenge_id=DB::table('challenges')->insertGetId($challenge);
    $data=array(
                    'challenge_id'       =>$challenge_id,
                    'challenge_name'     =>$_REQUEST['name'],
                    'challenge_timeline' =>$_REQUEST['challenge_timeline'],
                    'post_frequency'     =>$_REQUEST['post_frequency'],
                    'completion_reward'  =>$_REQUEST['completion_reward'],
                    'invite_friend'      =>$_REQUEST['invite_friend'],
                    'status'             =>1
                         );
    DB::table('user_challenges')->insert($data);
    return redirect()->back()->withFlashSuccess('Challenge successfully added') ;
  }
  public function editchallenge($id=NULL)
  {
        $challenges=DB::table('challenges')->join('user_challenges', 'challenges.id','=', 'user_challenges.challenge_id')->where('challenges.id',$id)->select('challenges.*', 'user_challenges.challenge_timeline', 'user_challenges.post_frequency', 'user_challenges.completion_reward', 'user_challenges.invite_friend') ->get();
        return view('backend.pages.editchallenge',compact('challenges') );
  }
  public function editchallengesave()
  {
      $challenge=array(
                        
                        'name'          =>$_REQUEST['name'],
                        'description'   =>$_REQUEST['description'],
                        'status'        =>1 );
    $challenge_id=DB::table('challenges')->where(['id'=> $_REQUEST['id']])->update($challenge);
    $data=array(
                    
                    'challenge_name'     =>$_REQUEST['name'],
                    'challenge_timeline' =>$_REQUEST['challenge_timeline'],
                    'post_frequency'     =>$_REQUEST['post_frequency'],
                    'completion_reward'  =>$_REQUEST['completion_reward'],
                    'invite_friend'      =>$_REQUEST['invite_friend'],
                    'status'             =>1
                         );
    DB::table('user_challenges')->where(['challenge_id'=> $_REQUEST['id']])->update($data);
    return redirect()->back()->withFlashSuccess('Challenge successfully updated') ;
  }
   public function deactivechallenge($id=NULL)
    {
        if(isset($id)){
            DB::table('challenges')->where('id',$id)->update(['status'=>0]);
             DB::table('user_challenges')->where('challenge_id',$id)->update(['status'=>0]);
            return redirect()->back()->withFlashSuccess('Challenge successfully deactivated');
        }else{
            return redirect()->back();
        }

    }
     public function activechallenge($id=NULL)
    {
        if(isset($id)){
             DB::table('challenges')->where('id',$id)->update(['status'=>1]);
             DB::table('user_challenges')->where('challenge_id',$id)->update(['status'=>1]);
           return redirect()->back()->withFlashSuccess('Challenge successfully activated');
        }else{return redirect()->back();}
    }
    public function deletechallenge($id=NULL)
    {
        if(isset($id))
            {
             DB::table('challenges')->where('id',$id)->get();
             DB::table('user_challenges')->where('challenge_id',$id)->get();
                return redirect()->back()->withFlashSuccess('Challenge successfully deleted');
            }else{
               return redirect()->back(); 
            }
    }
    public function viewchallenge($id=NULL)
    {
        $challenges=DB::table('challenges')->join('user_challenges', 'challenges.id','=', 'user_challenges.challenge_id')->where('challenges.id',$id)->select('challenges.*', 'user_challenges.challenge_timeline', 'user_challenges.post_frequency', 'user_challenges.completion_reward', 'user_challenges.invite_friend') ->get();
        return view('backend.pages.viewchallenge',compact('challenges') );
    }

}