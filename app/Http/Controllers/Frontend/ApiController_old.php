<?php namespace App\Http\Controllers\Frontend;

use DB,View,Session,Validator,Input,Redirect,Hash,Auth;
use Mail;
use App\Models\Access\User\User;
use App\Contactus;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Access\LoginRequest;
use App\Exceptions\GeneralException;
use Request;

 
/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class ApiController extends Controller {

	/**
	 * @return \Illuminate\View\View
	 */
	
public function index()
{
	javascript()->put([
		'test' => 'it works!'
	]);
	return view('frontend.index');
}

public function checkToken($access_token,$user_id='',$session_key='')
{
	$token=env('ACCESS_TOKEN');
	if($access_token!=$token){
		$resultArray['status']='0';
		$resultArray['message']='Invalid token!';
		return $resultArray;
		die;
	}
	else{
		if($user_id!=''){
			if($session_key==''){
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$session_key=substr(str_shuffle($chars),0,8);
				$checkuser=DB::table('mobile_session')->where('user_id',$user_id)->first();
				if(count($checkuser)>0){
					$update_arr= array('session_key' => $session_key );
					DB::table('mobile_session')->where('id',$checkuser->id)->update($update_arr);
				}
				else{
					$update_arr= array('session_key' => $session_key ,'user_id'=>$user_id);
					DB::table('mobile_session')->insert($update_arr);
				}
				$resultArray['status']='1';
				$resultArray['Data']['randnumber']=$session_key;
				return ($resultArray);
			}
			else{
				$con_arr=array('user_id'=>$user_id,'session_key'=>$session_key);
				$checkuser=DB::table('mobile_session')->where($con_arr)->first();
				if(count($checkuser)>0){
					$resultArray['status']='1';
					$resultArray['Data']['randnumber']=$session_key;
					return ($resultArray); die;
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid session';
					return ($resultArray); die;
				}
			}
		}	
		else{
			$resultArray['status']='1';
			$resultArray['Data']['message']='';
			return ($resultArray); die;
		}	
	}
}

public function subject(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($session_key) && !empty($session_key)))
	{
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else 
		{	
			$data = DB::table('subject')->where('status',1)->get();
			$resultArray['status']='1';
			if(count($data)>0){
				$resultArray['message']='Success';
				$result[]=array();
				$i=0;
				foreach($data as $subject){
					$result[$i]['id'] = $subject->id;
					$result[$i]['title'] = $subject->title;
					$result[$i]['description'] = $subject->description;
					//$result[$i]['status'] = $subject->status;
					++$i;
				} 
				$resultArray['Data']=$result;	
				return json_encode($resultArray);
			}
			else {
				$resultArray['message']='Record not found.';
				return json_encode($resultArray);
			}
		}
	} else 
	{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
	
}
public function request(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$subject_id = !empty($_REQUEST['subject_id']) ? $_REQUEST['subject_id']: "";
	$location = !empty($_REQUEST['location']) ? $_REQUEST['location'] : "";
	$duration = !empty($_REQUEST['duration']) ? $_REQUEST['duration'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($subject_id) && !empty($subject_id)) && (isset($location) && !empty($location)) && (isset($duration) && !empty($duration)) && (isset($session_key) && !empty($session_key))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$user = DB::table('requests')->insert([
										'user_id' =>$user_id, 
										'subject_id' => $subject_id,
										'location' => $location,
										'duration' => $duration
										]);
			$data = DB::table('requests')->where('user_id',$user_id)->orderBy('id', 'desc')->first();
			$resultArray['status']='1';
			$resultArray['message']='Success';
			$result['user_id'] = $data->user_id;
			$result['subject_id'] = $data->subject_id;
			$result['location'] = $data->location;
			$result['duration'] = $data->duration;
			$resultArray['Data']=$result;
			return json_encode($resultArray);
		}
	}
	else
	{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
public function updateprofile() 
{
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id']: "";
	$session_keys = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";	
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($id) && !empty($id)) && (isset($name) && !empty($name))&& (isset($session_keys) && !empty($session_keys))){

		$chkuserLogin = DB::table('users')->where('id',$id)->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,$id,$session_keys);
			if($check_auth['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{	
					$update_arr= array('name' => $name);
					DB::table('users')->where('id',$id)->update($update_arr);
					$chkuserLogin = DB::table('users')->where('id',$id)->first();
					$utype=2;
					if($chkuserLogin->usertype=="Student")
					{
						$utype=1;
					}	
					$resultArray['status']='1';
					$resultArray['message']='profile successfully updated';
					$resultArray['Data']['UserInfo']['id']=!empty($chkuserLogin->id) ? $chkuserLogin->id: "";
					$resultArray['Data']['UserInfo']['name']=!empty($chkuserLogin->name) ? $chkuserLogin->name: "";
					$resultArray['Data']['UserInfo']['username']=!empty($chkuserLogin->username) ? $chkuserLogin->username: "";
					$resultArray['Data']['UserInfo']['email']=!empty($email) ? $email: "";
					$resultArray['Data']['UserInfo']['usertype']=$utype;
					$resultArray['Data']['UserInfo']['dob']=!empty($chkuserLogin->dob) ? $chkuserLogin->dob: "";
					$resultArray['Data']['UserInfo']['address']=!empty($chkuserLogin->address) ? $chkuserLogin->address: "";
					$resultArray['Data']['UserInfo']['city']=!empty($chkuserLogin->city) ? $chkuserLogin->city: "";
					$resultArray['Data']['UserInfo']['state']=!empty($chkuserLogin->state) ? $chkuserLogin->state: "";
					$resultArray['Data']['UserInfo']['country']=!empty($chkuserLogin->country) ? $chkuserLogin->country: "";
					$resultArray['Data']['UserInfo']['phone1']=!empty($chkuserLogin->phone1) ? $chkuserLogin->phone1: "";
					$resultArray['Data']['UserInfo']['phone2']=!empty($chkuserLogin->phone2) ? $chkuserLogin->phone2: "";
					$resultArray['Data']['UserInfo']['phone3']=!empty($chkuserLogin->phone3) ? $chkuserLogin->phone3: "";
					$resultArray['Data']['UserInfo']['zip']=!empty($chkuserLogin->zip) ? $chkuserLogin->zip: "";
					$resultArray['Data']['UserInfo']['created_at']=!empty($chkuserLogin->created_at) ? $chkuserLogin->created_at: "";
					$resultArray['Data']['UserInfo']['updated_at']=!empty($chkuserLogin->updated_at) ? $chkuserLogin->updated_at: "";					
					$resultArray['Data']['UserInfo']['confirmed']=!empty($chkuserLogin->confirmed) ? $chkuserLogin->confirmed: "";					
					$resultArray['Data']['session_keys']=$session_keys;
					return json_encode($resultArray);								
				}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Invalid user';
			return json_encode($resultArray);			
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
public function request1(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key  = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id      = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$subject_id   = !empty($_REQUEST['subject_id']) ? $_REQUEST['subject_id']: "";
	$location     = !empty($_REQUEST['location']) ? $_REQUEST['location'] : "";
	$duration     = !empty($_REQUEST['duration']) ? $_REQUEST['duration'] : "";
	$lat          = !empty($_REQUEST['lat']) ? $_REQUEST['lat'] : "";
	$long         = !empty($_REQUEST['long']) ? $_REQUEST['long'] : "";
	$date         = !empty($_REQUEST['date']) ? $_REQUEST['date'] : "";
	$booking_type = !empty($_REQUEST['booking_type']) ? $_REQUEST['booking_type'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($subject_id) && !empty($subject_id)) && (isset($location) && !empty($location)) && (isset($duration) && !empty($duration)) && (isset($session_key) && !empty($session_key)) && (isset($date) && !empty($date)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$subject_store = $subject_id[0];
			$subject_arr = explode(',',$subject_id[0]);
			$subject_count = 0;
			foreach($subject_arr as $k=>$subject){
				$chkuserSubject = DB::table('subject')->where('id',$subject)->first();
				if(count($chkuserSubject)==0)
					$subject_count = ++$subject_count;
			}
			if($subject_count==0){
				$chkuserdata = DB::table('users')->where('usertype','Tutor')->get();
				$userdetailarray = array();
				$sub_store = array();
				if(count($chkuserdata)>0){
					foreach($chkuserdata as $key=>$users){ 
						foreach($subject_arr as $subjectarray){ 
							$userdetailarray= explode(',',$users->subject_id); 
							if(in_array($subjectarray, $userdetailarray)){ 
								$sub_store[]= !empty($users->id)?$users->id:"";
							}
						}
					}
					//subject with tutor
					if(count($sub_store)>0){
						if(isset($booking_type) && !empty($booking_type) && $booking_type=='fly'){
							$book_type = $booking_type;
						}
						else{
							$book_type = 'normal';
						}
						$parsedtime = date_parse($duration);
						$timeseconds = $parsedtime['hour'] * 3600 + $parsedtime['minute'] * 60 + $parsedtime['second'];
						$user = DB::table('requests')->insertGetId([
										'user_id' =>$user_id, 
										'subject_id' => $subject_store,
										'location' => $location,
										'latitude' => $lat,
										'longitude' => $long,
										'duration' => $timeseconds,
										'booking_type' => $book_type,
										'request_date' => date('Y-m-d H:i:s',strtotime($date))
										]);
						$subject_store = array();
						$userdetailarray = array();
						$sub_result = array_unique($sub_store);
						$sub_result = array_values($sub_result);
						foreach($sub_result as $i=>$sub_arr){
							$users = DB::table('users')->where('id',$sub_arr)->first();
							$userdetailarray[$i]['id'] =  	!empty($users->id)?$users->id:"";
							$userdetailarray[$i]['name'] =  !empty($users->name)?$users->name:"";
							$userdetailarray[$i]['username'] =  !empty($users->username)?$users->username:"";
							$userdetailarray[$i]['email'] = !empty($users->email)?$users->email:"";
							$userdetailarray[$i]['pic'] =   !empty($users->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$users->pic:"";
							$utype=2;
							if($users->usertype=="Student"){
								$utype=1;
							}
							$userdetailarray[$i]['usertype'] =   $utype;
							$userdetailarray[$i]['dob'] =   !empty($users->dob)?$users->dob:"";
							$userdetailarray[$i]['address'] =   !empty($users->address)?$users->address:"";
							$userdetailarray[$i]['city'] =   !empty($users->city)?$users->city:"";
							$userdetailarray[$i]['state'] =   !empty($users->state)?$users->state:"";
							$userdetailarray[$i]['country'] =   !empty($users->country)?$users->country:"";
							$userdetailarray[$i]['phone1'] =   !empty($users->phone1)?$users->phone1:"";
							$userdetailarray[$i]['phone2'] =   !empty($users->phone2)?$users->phone2:"";
							$userdetailarray[$i]['phone3'] =   !empty($users->phone3)?$users->phone3:"";
							$userdetailarray[$i]['zip'] =   !empty($users->zip)?$users->zip:"";
							if(!empty($users->id)){
								$ratetutor=DB::table('student_review')
									->where('tutor_id',$users->id)
							 		->select(
								      array(
								        DB::raw('round((SUM(rate)/COUNT(rate)),2) as ratecount')
								      )
								    )
									->groupBy('tutor_id')->first();
								if(count($ratetutor)>0){
									$userdetailarray[$i]['rate'] = $ratetutor->ratecount;
								}
								else{
									$userdetailarray[$i]['rate'] =  0;
								}
							}
							else{
								$userdetailarray[$i]['rate'] =  0;
							}
							$userdetailarray[$i]['confirmed'] =   !empty($users->confirmed)?$users->confirmed:"";
							$userdetailarray[$i]['created_at'] =   !empty($users->created_at)?$users->created_at:"";
							$userdetailarray[$i]['updated_at'] =   !empty($users->updated_at)?$users->updated_at:"";
							$tutor_set = DB::table('site_settings')->where('var_key','fees')->first();
							if(count($tutor_set)>0){
								$userdetailarray[$i][$tutor_set->var_key]=$tutor_set->var_value;
							}
							$subject_store = explode(',',$users->subject_id);
							$subjects='';
							foreach($subject_store as $key=>$sub)
							{
								$subdata = DB::table('subject')->where('id',$sub)->first();
								$subjects[$key]['id']=$subdata->id;
								$subjects[$key]['subject']=$subdata->title;
							}
							$userdetailarray[$i]['subjects']=$subjects;

						}
						
						$resultArray['status']='1';
						$resultArray['message']='Success!!!';
						$userdetailarray[$i]['booking_type'] = !empty($book_type)?$book_type:"";
						$resultArray['request_id'] =  	!empty($user)?$user:"";
						$resultArray['duration'] =  	!empty($timeseconds)?$timeseconds:"";
						$resultArray['Data']['tutorlist']=$userdetailarray;	

						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='this subject no tutor avaliable.';
						return json_encode($resultArray);
					}
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='No Tutor.';
					return json_encode($resultArray);
				}
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='Invalid subject.';
				return json_encode($resultArray);
			}
		}
	}
	else
	{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

public function userdetail(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($session_key) && !empty($session_key))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$data = DB::table('users')->where('id',$user_id)->orderBy('id', 'desc')->first();
			$resultArray['status']='1';
			$resultArray['message']='success';
			$resultArray['Data']['UserInfo']['id']=!empty($data->id)?$data->id:"";
			$resultArray['Data']['UserInfo']['name']=!empty($data->name)?$data->name:"";
			$resultArray['Data']['UserInfo']['username']=!empty($data->username)?$data->username:"";
			$utype=2;
			if($data->usertype=="Student"){
				$utype=1;
			}
			$resultArray['Data']['UserInfo']['email']=!empty($data->email)?$data->email:"";
			$resultArray['Data']['UserInfo']['pic']=!empty($data->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$data->pic:"";
			$resultArray['Data']['UserInfo']['usertype']=!empty($utype)?$utype:"";
			$resultArray['Data']['UserInfo']['dob']=!empty($data->dob)?$data->dob:"";
			$resultArray['Data']['UserInfo']['address']=!empty($data->address)?$data->address:"";
			$resultArray['Data']['UserInfo']['city']=!empty($data->city)?$data->city:"";
			$resultArray['Data']['UserInfo']['state']=!empty($data->state)?$data->state:"";
			$resultArray['Data']['UserInfo']['country']=!empty($data->country)?$data->country:"";
			$resultArray['Data']['UserInfo']['phone1']=!empty($data->phone1)?$data->phone1:"";
			$resultArray['Data']['UserInfo']['phone2']=!empty($data->phone2)?$data->phone2:"";
			$resultArray['Data']['UserInfo']['phone3']=!empty($data->phone3)?$data->phone3:"";
			$resultArray['Data']['UserInfo']['zip']=!empty($data->zip)?$data->zip:"";
			$resultArray['Data']['UserInfo']['created_at']=!empty($data->created_at)?$data->created_at:"";
			$resultArray['Data']['UserInfo']['updated_at']=!empty($data->updated_at)?$data->updated_at:"";
			$resultArray['Data']['UserInfo']['confirmed']=!empty($data->confirmed) ? $data->confirmed: "";
			$resultArray['Data']['session_keys']=$session_key;	
			$subjects = array();
			if($utype==2){
				$subject_idarray = explode(',',$data->subject_id);
				foreach($subject_idarray as $k=>$subject){ 
					$resultsubject = DB::table('subject')->where('id',$subject)->first();
					if(count($resultsubject)>0)
						$subjects[$k][$subject]=$resultsubject->title;
				} 
				$resultArray['Data']['UserInfo']['subjects']=$subjects;
				$tutor_set = DB::table('site_settings')->where('var_key','fees')->first();
				if(count($tutor_set)>0){
						$resultArray['Data']['UserInfo'][$tutor_set->var_key]=$tutor_set->var_value;
				}

			}
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

public function requestdetail(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$req_id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($session_key) && !empty($session_key)) && (isset($req_id) && !empty($req_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$data = DB::table('requests')->join('users', 'users.id', '=', 'requests.user_id')->where('requests.id',$req_id)->orderBy('requests.id', 'desc')->first();
			
			$resultArray['status']='1';
			$resultArray['message']='success';
			$resultArray['Data']['UserInfo']['req_id']=!empty($req_id)?$req_id:"";
			$resultArray['Data']['UserInfo']['location']=!empty($data->location)?$data->location:"";
			$resultArray['Data']['UserInfo']['duration']=!empty($data->duration)?$data->duration:"";
			$resultArray['Data']['UserInfo']['latitude']=!empty($data->latitude)?$data->latitude:"";
			$resultArray['Data']['UserInfo']['longitude']=!empty($data->longitude)?$data->longitude:"";
			$subjects = array();
			
				$subject_idarray = explode(',',$data->subject_id);
				foreach($subject_idarray as $k=>$subject){ 
					$resultsubject = DB::table('subject')->where('id',$subject)->first();
					if(count($resultsubject)>0)
						$subjects[$k][$subject]=$resultsubject->title;

					
				} 
				$resultArray['Data']['UserInfo']['subjects']=$subjects;			
			$resultArray['Data']['UserInfo']['user_id']=!empty($data->user_id)?$data->user_id:"";
			$resultArray['Data']['UserInfo']['username']=!empty($data->username)?$data->username:"";
			$utype=2;
			if($data->usertype=="Student"){
				$utype=1;
			}
			$resultArray['Data']['UserInfo']['email']=!empty($data->email)?$data->email:"";
			$resultArray['Data']['UserInfo']['pic']=!empty($data->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$data->pic:"";
			$resultArray['Data']['UserInfo']['usertype']=!empty($utype)?$utype:"";
			$resultArray['Data']['UserInfo']['dob']=!empty($data->dob)?$data->dob:"";
			$resultArray['Data']['UserInfo']['address']=!empty($data->address)?$data->address:"";
			$resultArray['Data']['UserInfo']['city']=!empty($data->city)?$data->city:"";
			$resultArray['Data']['UserInfo']['state']=!empty($data->state)?$data->state:"";
			$resultArray['Data']['UserInfo']['country']=!empty($data->country)?$data->country:"";
			$resultArray['Data']['UserInfo']['phone1']=!empty($data->phone1)?$data->phone1:"";
			$resultArray['Data']['UserInfo']['phone2']=!empty($data->phone2)?$data->phone2:"";
			$resultArray['Data']['UserInfo']['phone3']=!empty($data->phone3)?$data->phone3:"";
			$resultArray['Data']['UserInfo']['zip']=!empty($data->zip)?$data->zip:"";
			
			
			$resultArray['Data']['session_keys']=$session_key;
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/*public function login() 
{
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	$user_type = !empty($_REQUEST['user_type']) ? $_REQUEST['user_type'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($phone) && !empty($phone)) && (isset($user_type) && !empty($user_type)) && (isset($name) && !empty($name)) && (isset($email) && !empty($email))){
		$chkuserLogin = DB::table('users')->where('email',$email)->first();
		if(count($chkuserLogin)>0){
			$chkemailuser = DB::table('users')->where('email',$email)->where('username',$name)->first();
			if(count($chkemailuser)>0){
				$phone = str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $phone));
				if($chkuserLogin->phone1==$phone){
					$usertype='Tutor';
					if($user_type==1)
					{
						$usertype='Student';
					}
					if($chkuserLogin->usertype==$usertype){
						$update_arr= array('confirmed'=>0 ,'confirmation_code'=>'5yXkTG','updated_at'=>Date('Y-m-d H:i:s'));
						DB::table('users')->where('id',$chkuserLogin->id)->update($update_arr);
						$resultArray['status']='1';
						$resultArray['message']='Verification code has been sent on this number';
						$resultArray['Data']['verification_code']='5yXkTG';
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='You have already registerd with '.$chkuserLogin->usertype .' account';
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='This user is already registered with different phone number..';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='You have already registerd with '.$chkuserLogin->usertype .' account';
				return json_encode($resultArray);
			}
		}
		else{
			$chkusername = DB::table('users')->where('username',$name)->first();
			if(count($chkusername)>0){
				$resultArray['status']='0';
				$resultArray['message']='username is already registered';
				return json_encode($resultArray);
			}
			else{
				$usertype='Tutor';
				if($user_type==1)
				{
					$usertype='Student';
				}
				$phone = str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $phone));
				$update_arr= array('confirmed'=>0,'confirmation_code'=>'5yXkTG','phone1'=>$phone,'usertype'=>$usertype,'email'=>$email,'name'=>$name,'username'=>$name);
				User::insert($update_arr);
				$resultArray['status']='1';
				$resultArray['message']='Verification code has been sent on this number';
				$resultArray['Data']['confirmation_code']='5yXkTG';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
*/

public function studentregister() 
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	if((isset($email) && !empty($email)) && (isset($phone) && !empty($phone)) && (isset($name) && !empty($name))){
		$check_auth=$this->checkToken($access_token);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else{
			$chkuseremail=DB::table('users')->where('email',$email)->first();
			if(count($chkuseremail)>0){
				$resultArray['status']='0';
				$resultArray['message']='Email already Exist.';
				return json_encode($resultArray);
			}
			else{
				$chkuserphone=DB::table('users')->where('phone1',$phone)->first();
				if(count($chkuserphone)>0){
					$resultArray['status']='0';
					$resultArray['message']='Phone No already Exist.';
					return json_encode($resultArray);
				}
				else{
					$phone = str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $phone));
					//confirmation code
						//$confirmcharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
						//$confirmcharacters_code=substr(str_shuffle($confirmcharacters),0,8);
					//confirmation code
					$cheakuserverify=DB::table('user_verify')->where('email',$email)->first();
					if(count($cheakuserverify)>0){
						$up_verifycode= array('verification_code'=>'5yXkTG');
						DB::table('user_verify')->where('email',$email)->update($up_verifycode);
					}
					else{
						$in_verifycode= array('email'=>$email,'verification_code'=>'5yXkTG');
						DB::table('user_verify')->insert($in_verifycode);
					}
					$resultArray['status']='1';
					$resultArray['message']='Verify your account';
					$resultArray['Data']['name']=$name;
					$resultArray['Data']['phone']=$phone;
					$resultArray['Data']['email']=$email;
					$resultArray['Data']['verification_code']='5yXkTG';
					return json_encode($resultArray);
				}
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

public function studentregisterverify() 
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	$verification_code= !empty($_REQUEST['verification_code']) ? $_REQUEST['verification_code'] : "";
	if((isset($email) && !empty($email)) && (isset($phone) && !empty($phone)) && (isset($name) && !empty($name)) && (isset($verification_code) && !empty($verification_code))){
		$check_auth=$this->checkToken($access_token);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else{
			$chkuserverify=DB::table('user_verify')->where('email',$email)->first();
			if(count($chkuserverify)>0){
				if($chkuserverify->verification_code==$verification_code){
					$insertuser= array('name'=>$name,'username'=>$name,'email'=>$email,'usertype'=>'Student','phone1'=>$phone,'status'=>1,'confirmed'=>1);
					User::insert($insertuser);

					$resultArray['status']='1';
					$resultArray['message']='Student has been registered Successfully.';
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid Verification Code.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No verification code for user.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}


public function userLogin() 
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	if((isset($phone) && !empty($phone)) && (isset($access_token) && !empty($access_token))){
		$check_auth=$this->checkToken($access_token);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else{
			$phone = str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $phone));
			$chkphoneverify=DB::table('users')->where('phone1',$phone)->first();
			if(count($chkphoneverify)>0){
				$chkuserverify=DB::table('users')->where('phone1',$phone)->where('confirmed',1)->first();
				if(count($chkuserverify)>0){
					$update_verifycode= array('confirmation_code'=>'5yXkTG');
					DB::table('users')->where('phone1',$phone)->update($update_verifycode);

					$usertypeid="";
					if(!empty($chkuserverify->usertype)){
						if($chkuserverify->usertype=="Student")
							$usertypeid=1;
						elseif($chkuserverify->usertype=="Tutor")
							$usertypeid=2;
						else
							$usertypeid="";
					}
					$resultArray['status']='1';
					$resultArray['message']='Verify your login.';
					$resultArray['Data']['user_id']=!empty($chkuserverify->id)?$chkuserverify->id:"";
					$resultArray['Data']['usertype']=!empty($chkuserverify->usertype)?$chkuserverify->usertype:"";
					$resultArray['Data']['usertypeid']=!empty($usertypeid)?$usertypeid:"";
					$resultArray['Data']['email']=!empty($chkuserverify->email)?$chkuserverify->email:"";
					$resultArray['Data']['phone']=!empty($phone)?$phone:"";
					$resultArray['Data']['verification_code']='5yXkTG';
					return json_encode($resultArray);	
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Your account is not active.';
					return json_encode($resultArray);	
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid Phone number.';
				return json_encode($resultArray);	
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}


public function userVerify() 
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id']: "";
	$usertypeid = !empty($_REQUEST['usertypeid']) ? $_REQUEST['usertypeid']: "";
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email']: "";
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	$verification_code = !empty($_REQUEST['verification_code']) ? $_REQUEST['verification_code']: "";
	if((isset($access_token) && !empty($access_token)) && (isset($user_id) && !empty($user_id)) && (isset($usertypeid) && !empty($usertypeid)) && (isset($email) && !empty($email)) && (isset($phone) && !empty($phone)) && (isset($verification_code) && !empty($verification_code))){
		$chkuserverify=DB::table('users')->where('id',$user_id)->where('confirmed',1)->first();
		if(count($chkuserverify)>0){
			if($chkuserverify->confirmation_code!=""){
				if($chkuserverify->confirmation_code==$verification_code){
					$check_auth=$this->checkToken($access_token,$user_id);
					if($check_auth['status']!=1)
					{
						return json_encode($check_auth);
					}
					else{
						$update_verifycode= array('confirmation_code'=>"");
						DB::table('users')->where('id',$user_id)->update($update_verifycode);

						$resultArray['status']='1';
						$resultArray['message']='login successfully';
						$resultArray['Data']['UserInfo']['id']=!empty($chkuserverify->id)?$chkuserverify->id:"";
						$resultArray['Data']['UserInfo']['name']=!empty($chkuserverify->name)?$chkuserverify->name: "";
						$resultArray['Data']['UserInfo']['username']=!empty($chkuserverify->username)?$chkuserverify->username:"";
						$resultArray['Data']['UserInfo']['email']=!empty($email)?$email:"";
						$resultArray['Data']['UserInfo']['usertype']=$usertypeid;
						$resultArray['Data']['UserInfo']['dob']=!empty($chkuserverify->dob)?$chkuserverify->dob: "";
						$resultArray['Data']['UserInfo']['address']=!empty($chkuserverify->address)?$chkuserverify->address: "";
						$resultArray['Data']['UserInfo']['city']=!empty($chkuserverify->city) ? $chkuserverify->city: "";
						$resultArray['Data']['UserInfo']['state']=!empty($chkuserverify->state) ? $chkuserverify->state: "";
						$resultArray['Data']['UserInfo']['country']=!empty($chkuserverify->country) ? $chkuserverify->country: "";
						$resultArray['Data']['UserInfo']['phone1']=!empty($chkuserverify->phone1) ? $chkuserverify->phone1: "";
						$resultArray['Data']['UserInfo']['zip']=!empty($chkuserverify->zip) ? $chkuserverify->zip: "";
						$resultArray['Data']['UserInfo']['cre_at']=!empty($chkuserverify->created_at)?$chkuserverify->created_at: "";
						$resultArray['Data']['UserInfo']['upd_at']=!empty($chkuserverify->updated_at)?$chkuserverify->updated_at: "";
						$resultArray['Data']['session_keys']=$check_auth['Data']['randnumber'];
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Verification code does not match.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No verification code.';
				return json_encode($resultArray);
			}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Invalid user.';
			return json_encode($resultArray);	
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}



public function verify() 
{
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone']: "";
	$otpcode = !empty($_REQUEST['otpcode']) ? $_REQUEST['otpcode'] : "";
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$emails = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	if((isset($phone) && !empty($phone)) && (isset($otpcode) && !empty($otpcode))){
		$phone =str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $phone));
		$otpcode =str_replace('-', '',preg_replace('/[^A-Za-z0-9\-]/', '', $otpcode));
		$chkuserLogin = DB::table('users')->where('phone1',$phone)->where('email',$emails)->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,$chkuserLogin->id);
			if($check_auth['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{
				if($chkuserLogin->confirmation_code==$otpcode)
				{
					$update_arr= array('confirmed' => 1 ,'confirmation_code'=>'');
					DB::table('users')->where('id',$chkuserLogin->id)->update($update_arr);
					$utype=2;
					if($chkuserLogin->usertype=="Student")
					{
						$utype=1;
					}	
					$email=$chkuserLogin->email;
					if($chkuserLogin->email==$chkuserLogin->phone1)	
					{
						$email='';
					}				
					$resultArray['status']='1';
					$resultArray['message']='login successfully';
					$resultArray['Data']['UserInfo']['id']=!empty($chkuserLogin->id) ? $chkuserLogin->id: "";
					$resultArray['Data']['UserInfo']['name']=!empty($chkuserLogin->name) ? $chkuserLogin->name: "";
					$resultArray['Data']['UserInfo']['username']=!empty($chkuserLogin->username) ? $chkuserLogin->username: "";
					$resultArray['Data']['UserInfo']['email']=!empty($email) ? $email: "";
					$resultArray['Data']['UserInfo']['usertype']=$utype;
					$resultArray['Data']['UserInfo']['dob']=!empty($chkuserLogin->dob) ? $chkuserLogin->dob: "";
					$resultArray['Data']['UserInfo']['address']=!empty($chkuserLogin->address) ? $chkuserLogin->address: "";
					$resultArray['Data']['UserInfo']['city']=!empty($chkuserLogin->city) ? $chkuserLogin->city: "";
					$resultArray['Data']['UserInfo']['state']=!empty($chkuserLogin->state) ? $chkuserLogin->state: "";
					$resultArray['Data']['UserInfo']['country']=!empty($chkuserLogin->country) ? $chkuserLogin->country: "";
					$resultArray['Data']['UserInfo']['phone1']=!empty($chkuserLogin->phone1) ? $chkuserLogin->phone1: "";
					$resultArray['Data']['UserInfo']['phone2']=!empty($chkuserLogin->phone2) ? $chkuserLogin->phone2: "";
					$resultArray['Data']['UserInfo']['phone3']=!empty($chkuserLogin->phone3) ? $chkuserLogin->phone3: "";
					$resultArray['Data']['UserInfo']['zip']=!empty($chkuserLogin->zip) ? $chkuserLogin->zip: "";
					$resultArray['Data']['UserInfo']['created_at']=!empty($chkuserLogin->created_at) ? $chkuserLogin->created_at: "";
					$resultArray['Data']['UserInfo']['updated_at']=!empty($chkuserLogin->updated_at) ? $chkuserLogin->updated_at: "";					
					$resultArray['Data']['UserInfo']['confirmed']=!empty($chkuserLogin->confirmed) ? $chkuserLogin->confirmed: "";					
					$resultArray['Data']['session_keys']=$check_auth['Data']['randnumber'];
					return json_encode($resultArray);
				}
				else{					
					$resultArray['status']='0';
					$resultArray['message']='Invalid verification code ';
					return json_encode($resultArray);
				}
			}
		}
		else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid user';
					return json_encode($resultArray);		
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

public function logout() 
{
	$session_keys = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys']: "";
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($id) && !empty($id))){
		$chkuserLogin = DB::table('users')->where('id',$id)->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,'','');
			//print_r($check_auth); die;
			if($check_auth['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{				
					$update_arr= array('session_key' => '' );
					DB::table('mobile_session')->where('user_id',$id)->update($update_arr);
					$resultArray['status']='1';
					$resultArray['message']='User successfully logout';
					return json_encode($resultArray);				
				}
		}
		else{			
			$resultArray['status']='0';
			$resultArray['message']='Invalid user';
			return json_encode($resultArray);			
		}
	}
	else{
		$resultArray['Data'][0]['status']='0';
		$resultArray['Data'][0]['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
public function updateprofile1() 
{
	$subject_id = isset($_REQUEST['subjectid'])? $_REQUEST['subjectid']: "";
	//echo "<pre>";print_r($subject_id);exit;
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id']: "";
	$session_keys = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";	
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($id) && !empty($id)) && (isset($name) && !empty($name))&& (isset($session_keys) && !empty($session_keys))){
		$chkuserLogin = DB::table('users')->where('id',$id)->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,$id,$session_keys);
			if($check_auth['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{	
					$update_arr= array('name' => $name);
					DB::table('users')->where('id',$id)->update($update_arr);
					$chkuserLogin = DB::table('users')->where('id',$id)->first();
					$utype=2;
					if($chkuserLogin->usertype=="Student")
					{
						$utype=1;
					}	
					if($utype==2){
						if(isset($_REQUEST['subjectid']))
						{ 
							$subject_store = $subject_id[0];
							$subjectstore=array();
							$subject_arr= array('subject_id' => $subject_store);
							DB::table('users')->where('id',$id)->update($subject_arr);
							$subject_arrnew = explode(',',$subject_id[0]);
							foreach($subject_arrnew as $k=>$subjectname){
								$chksubject = DB::table('subject')->where('id',$subjectname)->first();
								if(count($chksubject)>0)
								$subjectstore[$k][$subjectname] = $chksubject->title;
							}
							if(count($subjectstore)==0){
								$update_arr= array('subject_id' => "0");
								DB::table('users')->where('id',$id)->update($update_arr);
							}
							$resultArray['Data']['UserInfo']['subjects']= $subjectstore;
						}
						else {
							$chksubject = $chkuserLogin->subject_id;
							$subject_store = explode(',',$chksubject);
							$subjectstore=array();
							foreach($subject_store as $k=>$subjectname){
								$chksubject = DB::table('subject')->where('id',$subjectname)->first();
								if(count($chksubject)>0)
									$subjectstore[$k][$subjectname] = $chksubject->title;
							} 
							$resultArray['Data']['UserInfo']['subjects'] = $subjectstore;
						}
					}
					$resultArray['status']='1';
					$resultArray['message']='profile successfully updated';
					$resultArray['Data']['UserInfo']['id']=!empty($chkuserLogin->id) ? $chkuserLogin->id: "";
					$resultArray['Data']['UserInfo']['name']=!empty($chkuserLogin->name) ? $chkuserLogin->name: "";
					$resultArray['Data']['UserInfo']['username']=!empty($chkuserLogin->username) ? $chkuserLogin->username: "";
					$resultArray['Data']['UserInfo']['email']=!empty($email) ? $email: "";
					$resultArray['Data']['UserInfo']['pic']=!empty($chkuserLogin->pic) ? "http://laravel.kart247.com/tutorondemand/public/images/users/".$chkuserLogin->pic: "";
					$resultArray['Data']['UserInfo']['usertype']=$utype;
					$resultArray['Data']['UserInfo']['dob']=!empty($chkuserLogin->dob) ? $chkuserLogin->dob: "";
					$resultArray['Data']['UserInfo']['address']=!empty($chkuserLogin->address) ? $chkuserLogin->address: "";
					$resultArray['Data']['UserInfo']['city']=!empty($chkuserLogin->city) ? $chkuserLogin->city: "";
					$resultArray['Data']['UserInfo']['state']=!empty($chkuserLogin->state) ? $chkuserLogin->state: "";
					$resultArray['Data']['UserInfo']['country']=!empty($chkuserLogin->country) ? $chkuserLogin->country: "";
					$resultArray['Data']['UserInfo']['phone1']=!empty($chkuserLogin->phone1) ? $chkuserLogin->phone1: "";
					$resultArray['Data']['UserInfo']['phone2']=!empty($chkuserLogin->phone2) ? $chkuserLogin->phone2: "";
					$resultArray['Data']['UserInfo']['phone3']=!empty($chkuserLogin->phone3) ? $chkuserLogin->phone3: "";
					$resultArray['Data']['UserInfo']['zip']=!empty($chkuserLogin->zip) ? $chkuserLogin->zip: "";
					$resultArray['Data']['UserInfo']['created_at']=!empty($chkuserLogin->created_at) ? $chkuserLogin->created_at: "";
					$resultArray['Data']['UserInfo']['updated_at']=!empty($chkuserLogin->updated_at) ? $chkuserLogin->updated_at: "";					
					$resultArray['Data']['UserInfo']['confirmed']=!empty($chkuserLogin->confirmed) ? $chkuserLogin->confirmed: "";					
					$resultArray['Data']['session_keys']=$session_keys;
					return json_encode($resultArray);								
				}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Invalid user';
			return json_encode($resultArray);			
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/*
public function tutordetail(){
	$subject_id = !empty($_REQUEST['subject']) ? $_REQUEST['subject']: "";
	$session_keys = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id']: "";
	$access_token = Request::header('accesstoken');
	if((isset($id) && !empty($id)) && (isset($subject_id) && !empty($subject_id)) && (isset($session_keys) && !empty($session_keys))){
		$check_auth=$this->checkToken($access_token,$id,$session_keys);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else{
			$chkuserSubject = DB::table('subject')->where('id',$subject_id)->first();
			if(count($chkuserSubject)>0){
				$chkuserdata = DB::table('users')->get();
				//echo "<pre>";print_r($chkuserSubject);exit;
				$userdetailarray = array();
				$i= 0;
				if(count($chkuserdata)>0){
					foreach($chkuserdata as $users){
						$subject_array = explode(',',$users->subject_id);
						if(in_array($subject_id, $subject_array)){
							$userdetailarray[$i]['name'] =  $users->name;
							$userdetailarray[$i]['email'] =  $users->email;
							$userdetailarray[$i]['dob'] =  $users->dob;
							++$i;
						}
					}
					if(count($userdetailarray)>0){
						$resultArray['status']='1';
						$resultArray['message']='success.';
						$resultArray['data'] = $userdetailarray;
						return json_encode($resultArray);
					}
					else {
						$resultArray['status']='0';
						$resultArray['message']='Subject is not assigned to any user.';
						return json_encode($resultArray);
					}
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='No user exist.';
					return json_encode($resultArray);
				}
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='Invalid subject';
				return json_encode($resultArray);
			}
			exit;
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}

} 
*/

public function updateimage() 
{
	//print_r(base64_decode($_REQUEST['pimage'])); die;
	$id = !empty($_REQUEST['id']) ? $_REQUEST['id']: "";
	$session_keys = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$pimage = !empty($_REQUEST['pimage']) ? $_REQUEST['pimage'] : "";	
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($id) && !empty($id)) && (isset($session_keys) && !empty($session_keys)) && (isset($pimage) && !empty($pimage))){
		$chkuserLogin = DB::table('users')->where('id',$id)->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,$id,$session_keys);
			if($check_auth['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{
					if($pimage!="")
					{
						$setimag=$this->base64toimage($pimage);
						if($setimag['status']==1 && $setimag['imagename']!='')
						{
							$update_arr= array('pic' => $setimag['imagename']);
							DB::table('users')->where('id',$id)->update($update_arr);
							$chkuserLogin = DB::table('users')->where('id',$id)->first();
						}
					}else{
						$chkuserLogin = DB::table('users')->where('id',$id)->first();
					}
					$images="";
					if($chkuserLogin->pic!="")
					{
						$images="http://laravel.kart247.com/tutorondemand/public/images/users/".$chkuserLogin->pic;
					}
					$resultArray['status']='1';
					$resultArray['message']='profile image successfully updated';
					$resultArray['Data']['UserInfo']['image']=$images;
					$resultArray['Data']['session_keys']=$session_keys;
					return json_encode($resultArray);								
				}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Invalid user';
			return json_encode($resultArray);			
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

public function base64toimage($base64)
{
	$data = base64_decode($base64); // base64 decoded image data
	$source_img = imagecreatefromstring($data);
	$rotated_img = imagerotate($source_img, 0, 10); // rotate with angle 90 here
	$fname= uniqid() . '.jpg';
	$file = 'images/users/'. $fname;
	$imageSave = imagejpeg($rotated_img, $file, 10);
	imagedestroy($source_img);
	/*
	define('UPLOAD_DIR', 'images/new/');
	$img = base64_decode($base64);
	$pos  = strpos($img, ';');
	$type = explode(':', substr($img, 0, $pos))[1];
	 $type = str_replace("image/", '', $type );
	
	//$img = str_replace("data:image/".$type.";base64,", '', $img);
	//$img = str_replace(' ', '+', $img);
	$data = ($img);
	$imagename=uniqid() . ".".$type;
	$file = UPLOAD_DIR . $imagename;
	$success = file_put_contents($file, $data);
	*/
	if($fname)
	{
		$returndata['status']=1;
		$returndata['imagename']=$fname;
	}
	else{
		$returndata['status']=0;
		$returndata['imagename']='';
	}
	return $returndata;

}




public function imagetobase64($imagename)
{
	define('UPLOAD_DIR', 'images/new/');
	$path = 'images/'.$imagename;
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data = file_get_contents($path);
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
	return $base64;

}
public function updateprofile12() 
{
		
	$this->base64toimage($base64);
	print_r($base64); die;exit;
	die;


	/*$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_key']) ? $_REQUEST['session_key'] : "";
	$access_token = !empty($_REQUEST['access_token']) ? $_REQUEST['access_token'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	$check_auth=$this->checkToken($access_token,$user_id,$session_key);
	if($check_auth['Data']['status']!=1){
		return json_encode($check_auth);
	}
	else{
		if((isset($name) && !empty($name) && isset($session_key) && !empty($session_key) & isset($user_id) && !empty($user_id) ) ){
			$update_arr= array('name' => $name);
			DB::table('users')->where('id',$user_id)->update($update_arr);
			$userInfo = DB::table('users')->where('id',$user_id)->first();
			$resultArray["Data"]["status"]="1";
			$resultArray["Data"]["message"]="Information Updated!";
			$resultArray['Data']['UserInfo']['id']=!empty($userInfo->id) ? $userInfo->id: "";
			$resultArray['Data']['UserInfo']['name']=!empty($userInfo->name) ? $userInfo->name: "";
			$resultArray['Data']['UserInfo']['username']=!empty($userInfo->username) ? $userInfo->username: "";
			$resultArray['Data']['UserInfo']['email']=!empty($userInfo->email) ? $userInfo->email: "";
			$resultArray['Data']['UserInfo']['usertype']=!empty($userInfo->usertype) ? $userInfo->usertype: "";
			$resultArray['Data']['UserInfo']['dob']=!empty($userInfo->dob) ? $userInfo->dob: "";
			$resultArray['Data']['UserInfo']['address']=!empty($userInfo->address) ? $userInfo->address: "";
			$resultArray['Data']['UserInfo']['city']=!empty($userInfo->city) ? $userInfo->city: "";
			$resultArray['Data']['UserInfo']['state']=!empty($userInfo->state) ? $userInfo->state: "";
			$resultArray['Data']['UserInfo']['country']=!empty($userInfo->country) ? $userInfo->country: "";
			$resultArray['Data']['UserInfo']['phone1']=!empty($userInfo->phone1) ? $userInfo->phone1: "";
			$resultArray['Data']['UserInfo']['phone2']=!empty($userInfo->phone2) ? $userInfo->phone2: "";
			$resultArray['Data']['UserInfo']['phone3']=!empty($userInfo->phone3) ? $userInfo->phone3: "";
			$resultArray['Data']['UserInfo']['zip']=!empty($userInfo->zip) ? $userInfo->zip: "";
			$resultArray['Data']['UserInfo']['created_at']=!empty($userInfo->created_at) ? $userInfo->created_at: "";
			$resultArray['Data']['UserInfo']['updated_at']=!empty($userInfo->updated_at) ? $userInfo->updated_at: "";
			return json_encode($resultArray);
		}
		else{
			$resultArray['Data'][0]['status']='0';
			$resultArray['Data'][0]['message']='field can\'t be empty.';
			return json_encode($resultArray);
		}
	}*/
}
//login
/*
//register
public function register()
{
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$password = !empty($_REQUEST['password']) ? $_REQUEST['password'] : "";
	$user_type = !empty($_REQUEST['user_type']) ? $_REQUEST['user_type'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	 $access_token = Request::header('accesstoken');
	  $access_token = !empty($access_token) ? $access_token : "";
	$check_auth=$this->checkToken($access_token);
	if($check_auth['Data']['status']!=1){
		return json_encode($check_auth);
	}
	else{
		if(isset($email) && !empty($email) && isset($password) && !empty($password) && isset($user_type) && !empty($user_type) && isset($name) && !empty($name)){
		$data  = Input::all();
		$chkuserLogin = DB::table('users')->where('email',$email)->first();
		if(!empty($chkuserLogin) && (isset($chkuserLogin))){
				$resultArray['Data']['status']='0';
				$resultArray['Data']['message']='Email already exist.';
				return json_encode($resultArray);
		}
		else{
			$confirmation_code =md5(uniqid(mt_rand(), 1));
			$user = User::create([
						'name' => $data['name'],
						'email' => $data['email'],
						'usertype' => $data['user_type'],
						'password' => Hash::make($data['password']),
						'confirmation_code' => $confirmation_code,
						'confirmed' =>  1 ,
					]);
					
			$user_data = array('email'=>$data['email'],'name' =>$data['name'],'token'=>$confirmation_code);
			/*Mail::send('emails.confirm', $user_data, function ($message) use ($user_data) {
				$message->to($user_data['email'], $user_data['name'])->subject('Registration activation form.');
			}); 
				
			$resultArray['Data']['status']='1';
			$resultArray['Data']['message']='Successfully registered.';
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['Data'][0]['status']='0';
		$resultArray['Data'][0]['message']='Invalid parameter.';
		return json_encode($resultArray);
	}
	}
	
}



public function login() 
{
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email']: "";
	$pwd = !empty($_REQUEST['password']) ? $_REQUEST['password'] : "";
	  $access_token = Request::header('accesstoken');
	  $access_token = !empty($access_token) ? $access_token : "";
	if((isset($email) && !empty($email)) && (isset($pwd) && !empty($pwd))){
		$chkuserLogin = DB::table('users')->where('email',$email)->select('id','name','username','email','usertype','dob','address','city','state','country','phone1','phone2','phone3','zip','created_at','updated_at','password')->first();
		if(count($chkuserLogin)>0){
			$check_auth=$this->checkToken($access_token,$chkuserLogin->id);
			if($check_auth['Data']['status']!=1)
			{
				return json_encode($check_auth);
			}
			else{
				if(count($chkuserLogin)>0 && Hash::check($pwd, $chkuserLogin->password)){
					$resultArray['Data']['status']='1';
					$resultArray['Data']['message']='login successfully';
					$resultArray['Data']['UserInfo']['id']=!empty($chkuserLogin->id) ? $chkuserLogin->id: "";
					$resultArray['Data']['UserInfo']['name']=!empty($chkuserLogin->name) ? $chkuserLogin->name: "";
					$resultArray['Data']['UserInfo']['username']=!empty($chkuserLogin->username) ? $chkuserLogin->username: "";
					$resultArray['Data']['UserInfo']['email']=!empty($chkuserLogin->email) ? $chkuserLogin->email: "";
					$resultArray['Data']['UserInfo']['usertype']=!empty($chkuserLogin->usertype) ? $chkuserLogin->usertype: "";
					$resultArray['Data']['UserInfo']['dob']=!empty($chkuserLogin->dob) ? $chkuserLogin->dob: "";
					$resultArray['Data']['UserInfo']['address']=!empty($chkuserLogin->address) ? $chkuserLogin->address: "";
					$resultArray['Data']['UserInfo']['city']=!empty($chkuserLogin->city) ? $chkuserLogin->city: "";
					$resultArray['Data']['UserInfo']['state']=!empty($chkuserLogin->state) ? $chkuserLogin->state: "";
					$resultArray['Data']['UserInfo']['country']=!empty($chkuserLogin->country) ? $chkuserLogin->country: "";
					$resultArray['Data']['UserInfo']['phone1']=!empty($chkuserLogin->phone1) ? $chkuserLogin->phone1: "";
					$resultArray['Data']['UserInfo']['phone2']=!empty($chkuserLogin->phone2) ? $chkuserLogin->phone2: "";
					$resultArray['Data']['UserInfo']['phone3']=!empty($chkuserLogin->phone3) ? $chkuserLogin->phone3: "";
					$resultArray['Data']['UserInfo']['zip']=!empty($chkuserLogin->zip) ? $chkuserLogin->zip: "";
					$resultArray['Data']['UserInfo']['created_at']=!empty($chkuserLogin->created_at) ? $chkuserLogin->created_at: "";
					$resultArray['Data']['UserInfo']['updated_at']=!empty($chkuserLogin->updated_at) ? $chkuserLogin->updated_at: "";					
					$resultArray['Data']['session_keys']=$check_auth['Data']['randnumber'];
					return json_encode($resultArray);
				}
				else{
					$resultArray['Data']['status']='0';
					$resultArray['Data']['message']='User not matched!';
					return json_encode($resultArray);
				}
			}
		}
		else{
			$resultArray['Data']['status']='0';
			$resultArray['Data']['message']='User not matched!';
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['Data'][0]['status']='0';
		$resultArray['Data'][0]['message']='Please enter email & password!';
		return json_encode($resultArray);
	}
}

//forget password
public function forgotpassword()
{
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	 $access_token = Request::header('accesstoken');
	  $access_token = !empty($access_token) ? $access_token : "";
	$check_auth=$this->checkToken($access_token);
	if($check_auth['Data']['status']!=1){
		return json_encode($check_auth);
	}
	else{
		if(isset($_REQUEST['email']) && !empty($_REQUEST['email'])){
		$check = DB::table('users')->where('email',$email)->first();
		if(!empty($check)){
			$resultArray['Data']['status']='1';
			$resultArray['Data']['message']='Please reset your password!';
			$resultArray['Data']['email']=$check->email;
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$passeord=substr(str_shuffle($chars),0,8);				
			$resetpass=Hash::make($passeord);
			$update_arr= array('password' => $resetpass );
			DB::table('users')->where('id',$check->id)->update($update_arr);
			$user_data = array('email'=>$check->email,'name' => $check->name, 'resetpass' =>$passeord);
			Mail::send('emails.forgot', $user_data, function ($message) use ($check) {
				$message->to($check->email, $check->name)->subject('Password reset instructions from');
			});
			$sendmsg['Data']['status']='1';
			$sendmsg['Data']['message']='Please check your email for reset instructions!';
			$sendmsg['Data']['user_id']=$check->id;
			return json_encode($sendmsg);				
		}
		else{
			$resultArray["Data"]["status"]="0";
			$resultArray["Data"]["message"]="Email doesn't match!";
			return json_encode($resultArray);			
		}
	}
	else{
		$resultArray["Data"]["status"]="0";
		$resultArray["Data"]["message"]="Please enter email!";	
		return json_encode($resultArray);
	}
	}
	
}	

//change password
public function changePass()
{
 	$access_token = Request::header('accesstoken');
  	$access_token = !empty($access_token) ? $access_token : "";
	$old_password = !empty($_REQUEST['old_password']) ? $_REQUEST['old_password'] : "";
	$new_password = !empty($_REQUEST['password']) ? $_REQUEST['password'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_key']) ? $_REQUEST['session_key'] : "";
	$check_auth=$this->checkToken($access_token,$user_id,$session_key);
	if($check_auth['Data']['status']!=1){
		return json_encode($check_auth);
	}
	else{
		if(!empty($user_id) && isset($user_id) && !empty($_REQUEST['old_password']) && isset($_REQUEST['old_password']) && !empty($_REQUEST['password']) && isset($_REQUEST['password']) && !empty($session_key) && isset($session_key) ){
			$chkuserLogin = DB::table('users')->where('id',$user_id)->first();
			if(!empty($chkuserLogin)  && Hash::check($old_password, $chkuserLogin->password))
			{
				$password = Hash::make($new_password);
				$update_arr= array('password' => $password );
				DB::table('users')->where('id',$user_id)->update($update_arr);
				$userInfo = DB::table('users')->where('id',$user_id)->first();
				$resultArray["Data"]["status"]="1";
				$resultArray["Data"]["message"]="Password Successfully Updated!";
				//$resultArray["Data"]["UserInfo"]=$userInfo;
				return json_encode($resultArray);					
			}
			else{
				$resultArray["Data"]["status"]="0";
				$resultArray["Data"]["message"]="Old Password Does Not exists!";
				return json_encode($resultArray);
			} 
		}
		else{
			$resultArray["Data"]["status"]="0";
			$resultArray["Data"]["message"]="Please Enter Old Password and New Password";
			return json_encode($resultArray);
		}
	}
}*/

//change profile
//check email
public function chkemail() 
{	
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	if(isset($email) && !empty($email)){			
		$chkuserLogin = DB::table('users')->where('email',$email)->first();
		if(!empty($chkuserLogin) && (isset($chkuserLogin))){				
			$resultArray['Result']['status']='1';
			$resultArray['Result']['message']='Email matched';
			return json_encode($resultArray);
		}
		else {
			$resultArray['Result']['status']='0';
			$resultArray['Result']['message']='Email does not matched!';
			return json_encode($resultArray);
		}
	}
	else {
		$resultArray['Result'][0]['status']='0';
		$resultArray['Result'][0]['message']='Please Enter Email!';
		return json_encode($resultArray);
	}
}





/* --------- assign request -------------- */
public function assignrequest(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$tutor_id = !empty($_REQUEST['tutor_id']) ? $_REQUEST['tutor_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : ""; //
	if((isset($user_id) && !empty($user_id)) && (isset($session_key) && !empty($session_key)) && (isset($tutor_id) && !empty($tutor_id)) && (isset($request_id) && !empty($request_id)) && isset($_REQUEST['status']) ){
	
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$tutor_req = DB::table('users')->where('id',$tutor_id)->where('usertype','Tutor')->first();
			if(count($tutor_req)>0){
				$requestdata = DB::table('assign_requests')->where('request_id',$request_id)->first();
				if(count($requestdata)>0){
					$resultArray['status']='0';
					$resultArray['message']='Request already assigned.';
					return json_encode($resultArray);
				}
				else {
					$req = DB::table('assign_requests')->insert([
										'tutor_id' =>$tutor_id,
										'user_id' =>$user_id,
										'request_id' => $request_id,
										'status' => $_REQUEST['status'],
										'created_at' => Date('Y-m-d H:i:s'),
										'updated_at' => Date('Y-m-d H:i:s')
										]);
					
					$msg='';
					if($_REQUEST['status']==0)
					{
						$msg='request has canceled';
					}
					elseif($_REQUEST['status']==1)
					{
						$msg='request is accepted';
					}
					if($_REQUEST['status']==2)
					{
						$msg='request is pending';
					}
					//notification for user
					$abc = DB::table('notifications')->insert([
										'user_id' =>$user_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>$msg,
										'created_at'=>date('Y-m-d H:i:s'),
										'updated_at'=>date('Y-m-d H:i:s')
										]);
					//notification for user
					//notification for user
					$xyz = DB::table('notifications')->insert([
										'user_id' =>$tutor_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>'You have a new request.',
										'created_at'=>date('Y-m-d H:i:s'),
										'updated_at'=>date('Y-m-d H:i:s')
										]);

					//notification for user
					//notification

					$data = DB::table('assign_requests')->where('user_id',$user_id)->where('request_id',$request_id)->orderBy('id', 'desc')->first();
					$resultArray['status']='1';
					$resultArray['message']='request has been assigned.';
					$resultArray['request_status'] = $data->status;
					return json_encode($resultArray);	
				}
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='No tutor Exist.';
				return json_encode($resultArray);	
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- assign request -------------- */
/* --------- Request Accept -------------- */
public function requestaccept(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$status = !empty($_REQUEST['status']) ? $_REQUEST['status'] : "";	
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id)) && isset($_REQUEST['status']) )  {
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$tutor_req = DB::table('assign_requests')->where('tutor_id',$user_id)->where('request_id',$request_id)->first();
			if(count($tutor_req)>0){
				$update_arr= array('status' => $status,'updated_at'=>Date('Y-m-d H:i:s'));
				DB::table('assign_requests')->where('tutor_id',$user_id)->where('request_id',$request_id)->update($update_arr);
				
				//notification
					$msg='';
					if($_REQUEST['status']==0)
					{
						$msg='request has been canceled';
					}
					elseif($_REQUEST['status']==1)
					{
						$msg='request is accepted';
					}
					if($_REQUEST['status']==2)
					{
						$msg='request is pending';
					}
					$abc = DB::table('notifications')->insert([
										'user_id' =>$tutor_req->user_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>$msg,
										'created_at'=>date('Y-m-d H:i:s'),
										'updated_at'=>date('Y-m-d H:i:s')
										]);


					//notification

				$resultArray['status']='1';
				$resultArray['message']='status has been changed.';
				$resultArray['Data']['status']=$status;
				$resultArray['Data']['assign_request_id']=$tutor_req->id;
				return json_encode($resultArray);
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='Request does not exist.';
				return json_encode($resultArray);
			}
			
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Request Accept -------------- */
/* --------- Request User -------------- */
public function requestuser(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "";
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) )
	{
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$student_req = DB::table('users')->where('id',$user_id)->first();
			if(count($student_req)>0)
			{
				$stu_req = DB::table('requests')->where('user_id',$user_id)->lists('id');
				if(count($stu_req)>0)
				{
					$i=0;
					$result_store_array=array();
					if(isset($_REQUEST['status'])){
						foreach($stu_req as $key=>$sr)
						{
							$results = 	DB::table('requests')
										->where('requests.id', '=', $sr)
										->join('assign_requests', function($join)
											{
												$join->on('requests.id', '=', 'assign_requests.request_id');
											}
										)
										->where('assign_requests.status', '=', $status)
										->get();
							if(count($results)>0)
							{
								foreach($results as $result)
								{
									$result_store_array[$i]['tutor_id'] = $result->tutor_id;
									if(!empty($result->tutor_id)){
										$tudet = DB::table('users')->where('id',$result->tutor_id)->where('status',1)->first();
										if(count($tudet)){
											$result_store_array[$i]['tutor_name']=!empty($tudet->name)?$tudet->name:"";
											$result_store_array[$i]['tutor_username']=!empty($tudet->username)?$tudet->username:"";
										}
									}
									//favourites
									$tutor_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$result->tutor_id)->where('status',1)->first();
									if(count($tutor_fav)>0){
										$result_store_array[$i]['favourites'] = 1;
									}
									else{
										$result_store_array[$i]['favourites'] = 0;
									}
									//favourites
									//tutor image 
									$tutor_img = DB::table('users')->where('id',$result->tutor_id)->first();
									$result_store_array[$i]['tutor_pic']=!empty($tutor_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$tutor_img->pic:"";
									//tutor image 
									$result_store_array[$i]['location'] = $result->location;
									$result_store_array[$i]['duration'] = $result->duration;
									$result_store_array[$i]['request_id'] = $result->request_id;
									$result_store_array[$i]['request_date'] = $result->request_date;
									$result_store_array[$i]['status'] = $result->status;
									if($result->status==0)
										$result_store_array[$i]['status_message']='Request Cancel.';
									elseif($result->status==1)
										$result_store_array[$i]['status_message']='Request Accept.';
									elseif($result->status==2)
										$result_store_array[$i]['status_message']='Request Pending.';
									elseif($result->status==3)
										$result_store_array[$i]['status_message']='Request Complete.';
									$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
									$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
									//$result_store_array[$i]['subject_id'] = $result->subject_id;
									$subject_idarray = explode(',',$result->subject_id);
									$subjects=array();
									foreach($subject_idarray as $k=>$subject)
									{ 
										$resultsubject = DB::table('subject')->where('id',$subject)->first();
										if(count($resultsubject)>0){
											$subjects[$k]['subject_name']=$resultsubject->title;
											$subjects[$k]['subject_id']=$subject;
										}
									} 
									$result_store_array[$i]['subjects']=$subjects;
									$result_store_array[$i]['created_at']=!empty($result->created_at)?$result->created_at:"";
									$result_store_array[$i]['updated_at']=!empty($result->updated_at)?$result->updated_at:"";
									++$i;
								}
							}
						}

					}
					else{
						$results = 	DB::table('requests')
									->whereIn('requests.id',$stu_req)
									->join('assign_requests', function($join)
										{
											$join->on('requests.id', '=', 'assign_requests.request_id');
										}
									)
									->where('requests.user_id', '=', $user_id)
								->get();
						if(count($results)>0){
							foreach($results as $result){
								$result_store_array[$i]['tutor_id'] = $result->tutor_id;
								if(!empty($result->tutor_id)){
										$tudet = DB::table('users')->where('id',$result->tutor_id)->where('status',1)->first();
										if(count($tudet)){
											$result_store_array[$i]['tutor_name']=!empty($tudet->name)?$tudet->name:"";
											$result_store_array[$i]['tutor_username']=!empty($tudet->username)?$tudet->username:"";
										}
								}
								//favourites
								$tutor_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$result->tutor_id)->where('status',1)->first();
								if(count($tutor_fav)>0){
									$result_store_array[$i]['favourites'] = 1;
								}
								else{
									$result_store_array[$i]['favourites'] = 0;
								}
								//favourites
								//tutor image 
								$tutor_img = DB::table('users')->where('id',$result->tutor_id)->first();
								$result_store_array[$i]['tutor_pic']=!empty($tutor_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$tutor_img->pic:"";
								//tutor image
								$result_store_array[$i]['location'] = $result->location;
								$result_store_array[$i]['duration'] = $result->duration;
								$result_store_array[$i]['request_id'] = $result->request_id;
								$result_store_array[$i]['request_date'] = $result->request_date;
								$result_store_array[$i]['status'] = $result->status;
								if($result->status==0)
									$result_store_array[$i]['status_message']='Request Cancel.';
								elseif($result->status==1)
									$result_store_array[$i]['status_message']='Request Accept.';
								elseif($result->status==2)
									$result_store_array[$i]['status_message']='Request Pending.';
								elseif($result->status==3)
									$result_store_array[$i]['status_message']='Request Complete.';
								$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
								$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
								//$result_store_array[$i]['subject_id'] = $result->subject_id;
								
								$subject_idarray = explode(',',$result->subject_id);
								$subjects=array();
								foreach($subject_idarray as $k=>$subject){ 
									$resultsubject = DB::table('subject')->where('id',$subject)->first();
									if(count($resultsubject)>0){
										//$subjects[$k][$subject]=$resultsubject->title;
										$subjects[$k]['subject_id']=$subject;
										$subjects[$k]['subject_name']=$resultsubject->title;
									}
								} 
								$result_store_array[$i]['subjects']=$subjects;
								$result_store_array[$i]['created_at']=!empty($result->created_at)?$result->created_at:"";
								$result_store_array[$i]['updated_at']=!empty($result->updated_at)?$result->updated_at:"";
								++$i;
							}
						}
							
					}
					if(count($result_store_array)>0){
						$resultArray['status']='1';
						$resultArray['message']='Success.';
						$resultArray['Data']=$result_store_array;
						return json_encode($resultArray);
					}
					else{
							$resultArray['status']='0';
							$resultArray['message']='No history';
							return json_encode($resultArray);
					}
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='No Request exist for User.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='User does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*public function requestuser(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "";
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) )
	{
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$student_req = DB::table('users')->where('id',$user_id)->first();
			if(count($student_req)>0)
			{
				$stu_req = DB::table('requests')->where('user_id',$user_id)->get();
				if(count($stu_req)>0)
				{
					$i=0;
					$result_store_array=array();
					if(isset($_REQUEST['status'])){
						foreach($stu_req as $key=>$sr)
						{
							$results = 	DB::table('requests')
										->where('requests.id', '=', $sr->id)
										->join('assign_requests', function($join)
											{
												$join->on('requests.id', '=', 'assign_requests.request_id');
											}
										)
										->where('assign_requests.status', '=', $status)
										->get();
							if(count($results)>0)
							{
								foreach($results as $result)
								{
									$result_store_array[$i]['tutor_id'] = $result->tutor_id;
									//favourites
									$tutor_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$result->tutor_id)->where('status',1)->first();
									if(count($tutor_fav)>0){
										$result_store_array[$i]['favourites'] = 1;
									}
									else{
										$result_store_array[$i]['favourites'] = 0;
									}
									//favourites
									//tutor image 
									$tutor_img = DB::table('users')->where('id',$result->tutor_id)->first();
									$result_store_array[$i]['tutor_pic']=!empty($tutor_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$tutor_img->pic:"";
									//tutor image 
									$result_store_array[$i]['location'] = $result->location;
									$result_store_array[$i]['duration'] = $result->duration;
									$result_store_array[$i]['request_id'] = $result->request_id;
									$result_store_array[$i]['status'] = $result->status;
									if($result->status==0)
										$result_store_array[$i]['status_message']='Request Cancel.';
									elseif($result->status==1)
										$result_store_array[$i]['status_message']='Request Accept.';
									elseif($result->status==2)
										$result_store_array[$i]['status_message']='Request Pending.';
									elseif($result->status==3)
										$result_store_array[$i]['status_message']='Request Complete.';
									$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
									$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
									//$result_store_array[$i]['subject_id'] = $result->subject_id;
									$subject_idarray = explode(',',$result->subject_id);
									$subjects=array();
									foreach($subject_idarray as $k=>$subject)
									{ 
										$resultsubject = DB::table('subject')->where('id',$subject)->first();
										if(count($resultsubject)>0){
											$subjects[$k]['subject_name']=$resultsubject->title;
											$subjects[$k]['subject_id']=$subject;
										}
									} 
									$result_store_array[$i]['subjects']=$subjects;

									++$i;
								}
							}
						}

					}
					else{
						foreach($stu_req as $key=>$sr){
							$result = 	DB::table('requests')
										->where('requests.id', '=', $sr->id)
										->join('assign_requests', function($join)
											{
												$join->on('requests.id', '=', 'assign_requests.request_id');
											}
										)
									->first(); 
							if(count($result)>0){
								$result_store_array[$i]['tutor_id'] = $result->tutor_id;
								//favourites
									$tutor_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$result->tutor_id)->where('status',1)->first();
									if(count($tutor_fav)>0){
										$result_store_array[$i]['favourites'] = 1;
									}
									else{
										$result_store_array[$i]['favourites'] = 0;
									}
									//favourites
								//tutor image 
								$tutor_img = DB::table('users')->where('id',$result->tutor_id)->first();
								$result_store_array[$i]['tutor_pic']=!empty($tutor_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$tutor_img->pic:"";
								//tutor image 
								$result_store_array[$i]['location'] = $result->location;
								$result_store_array[$i]['duration'] = $result->duration;
								$result_store_array[$i]['request_id'] = $result->request_id;
								$result_store_array[$i]['status'] = $result->status;
								if($result->status==0)
									$result_store_array[$i]['status_message']='Request Cancel.';
								elseif($result->status==1)
									$result_store_array[$i]['status_message']='Request Accept.';
								elseif($result->status==2)
									$result_store_array[$i]['status_message']='Request Pending.';
								elseif($result->status==3)
									$result_store_array[$i]['status_message']='Request Complete.';
								$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
								$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
								//$result_store_array[$i]['subject_id'] = $result->subject_id;
								
								$subject_idarray = explode(',',$result->subject_id);
								$subjects=array();
								foreach($subject_idarray as $k=>$subject){ 
									$resultsubject = DB::table('subject')->where('id',$subject)->first();
									if(count($resultsubject)>0){
										//$subjects[$k][$subject]=$resultsubject->title;
										$subjects[$k]['subject_id']=$subject;
										$subjects[$k]['subject_name']=$resultsubject->title;
									}
								} 
								$result_store_array[$i]['subjects']=$subjects;
								++$i;
							}
						}
					}
					if(count($result_store_array)>0){
						$resultArray['status']='1';
						$resultArray['message']='Success.';
						$resultArray['Data']=$result_store_array;
						return json_encode($resultArray);
					}
					else{
							$resultArray['status']='0';
							$resultArray['message']='No history';
							return json_encode($resultArray);
					}
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='No Request exist for User.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='User does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}*/
/* --------- Request User -------------- */


/* --------- Request Tutor -------------- */
public function requesttutor(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "";
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$student_req = DB::table('users')->where('id',$user_id)->where('usertype','Tutor')->first();
			if(count($student_req)>0){
				$tut_req = DB::table('assign_requests')->where('tutor_id',$user_id)->first();
				if(count($tut_req)>0){
					$i=0;
					$result_store_array=array();
					if(isset($_REQUEST['status'])){
						$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
										$join->on('assign_requests.user_id', '=', 'requests.user_id');
									}
								)
								->where('assign_requests.tutor_id', '=', $user_id)
								->where('assign_requests.status', '=', $status)
								->get(); 
						if(count($results)>0){
							foreach($results as $result){ 
										$result_store_array[$i]['user_id'] = $result->user_id;
										//favourites
										$user_favourites = DB::table('user_favourites')
															->where('user_id',$result->user_id)
															->where('tutor_id',$user_id)
															->where('status',1)
															->first();
										if(count($user_favourites)>0)
											$result_store_array[$i]['favourites'] = 1;
										else
											$result_store_array[$i]['favourites'] = 0;
										//favourites
										//user image 
									$user_img = DB::table('users')->where('id',$result->user_id)->first();
									if(count($user_img)>0){
										$result_store_array[$i]['user_pic']=!empty($user_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_img->pic:"";
										$result_store_array[$i]['name']=!empty($user_img->name)?$user_img->name:"";
										$result_store_array[$i]['user_name']=!empty($user_img->username)?$user_img->username:"";
									}
									
								//user image 
										$result_store_array[$i]['location'] = $result->location;
										$result_store_array[$i]['duration'] = $result->duration;
										$result_store_array[$i]['request_id'] = $result->request_id;
										$result_store_array[$i]['request_date'] = $result->request_date;
										$result_store_array[$i]['status'] = $result->status;
										if($result->status==0)
											$result_store_array[$i]['status_message']='Request Cancel.';
										elseif($result->status==1)
											$result_store_array[$i]['status_message']='Request Accept.';
										elseif($result->status==2)
											$result_store_array[$i]['status_message']='Request Pending.';
										elseif($result->status==3)
											$result_store_array[$i]['status_message']='Request Complete.';
										$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
										$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
										//$result_store_array[$i]['subject_id'] = $result->subject_id;
										
										$subject_idarray = explode(',',$result->subject_id);
										$subjects=array();
										foreach($subject_idarray as $k=>$subject){ 
											$resultsubject = DB::table('subject')->where('id',$subject)->first();
											if(count($resultsubject)>0){
												//$subjects[$k][$subject]=$resultsubject->title;
												$subjects[$k]['subject_id']=$subject;
												$subjects[$k]['subject_name']=$resultsubject->title;
											}
												
										} 
										$result_store_array[$i]['subjects']=$subjects;
										$result_store_array[$i]['created_at']=!empty($result->created_at)?$result->created_at:"";
										$result_store_array[$i]['updated_at']=!empty($result->updated_at)?$result->updated_at:"";
										++$i;
							}
						}
					}
					else{

						$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
										$join->on('assign_requests.user_id', '=', 'requests.user_id');
									}
								)
								->where('assign_requests.tutor_id', '=', $user_id)
								->get(); 
					if(count($results)>0){
						foreach($results as $result){ 
								$result_store_array[$i]['user_id'] = $result->user_id;
								//favourites
								$user_favourites = DB::table('user_favourites')
													->where('user_id',$result->user_id)
													->where('tutor_id',$user_id)
													->where('status',1)
													->first();
								if(count($user_favourites)>0)
									$result_store_array[$i]['favourites'] = 1;
								else
									$result_store_array[$i]['favourites'] = 0;
								//favourites
								//user image 
								$user_img = DB::table('users')->where('id',$result->user_id)->first();
								if(count($user_img)>0){
									$result_store_array[$i]['user_pic']=!empty($user_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_img->pic:"";
									$result_store_array[$i]['name']=!empty($user_img->name)?$user_img->name:"";
									$result_store_array[$i]['user_name']=!empty($user_img->username)?$user_img->username:"";
								}
								//user image 

								$result_store_array[$i]['location'] = $result->location;
								$result_store_array[$i]['duration'] = $result->duration;
								$result_store_array[$i]['request_id'] = $result->request_id;
								$result_store_array[$i]['status'] = $result->status;
								if($result->status==0)
									$result_store_array[$i]['status_message'] ='Request Cancel.';
								elseif($result->status==1)
									$result_store_array[$i]['status_message'] ='Request Accept.';
								elseif($result->status==2)
									$result_store_array[$i]['status_message'] ='Request Pending.';
								elseif($result->status==3)
									$result_store_array[$i]['status_message'] ='Request Complete.';
								$result_store_array[$i]['request_date'] = $result->request_date;
								$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
								$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
								//$result_store_array[$i]['subject_id'] = $result->subject_id;
								
								$subject_idarray = explode(',',$result->subject_id);
								$subjects=array();
								foreach($subject_idarray as $k=>$subject){ 
									$resultsubject = DB::table('subject')->where('id',$subject)->first();
									if(count($resultsubject)>0){
										//$subjects[$k][$subject]=$resultsubject->title;
										$subjects[$k]['subject_id']=$subject;
										$subjects[$k]['subject_name']=$resultsubject->title;
									}
								} 
								$result_store_array[$i]['subjects']=$subjects;
								$result_store_array[$i]['created_at']=!empty($result->created_at)?$result->created_at:"";
								$result_store_array[$i]['updated_at']=!empty($result->updated_at)?$result->updated_at:"";
								++$i;
						}
					}
					else{
							$resultArray['status']='0';
							$resultArray['message']='No Request found.';
							return json_encode($resultArray);
					}
					
					}
					if(count($result_store_array)>0){
						$resultArray['status']='1';
						$resultArray['message']='Success.';
						$resultArray['Data']=$result_store_array;
						return json_encode($resultArray);
					}
					else{
							$resultArray['status']='0';
							if($status==0){
								$resultArray['message']='No Request found. Inactive Status.';
							}
							elseif($status==1){
								$resultArray['message']='No Request found. Active Status.';
							}
							elseif($status==2){
								$resultArray['message']='No Request found. Pending Status.';
							}
							elseif($status==3){
								$resultArray['message']='No Request found. Complete Status.';
							}
							else{
								$resultArray['message']='No status found.';
							}
							return json_encode($resultArray);
					}	
					$resultArray['status']='1';
					$resultArray['message']='Success.';
					$resultArray['Data']=$result_store_array;
					return json_encode($resultArray);
							
							
				} 
				else {
					$resultArray['status']='0';
					$resultArray['message']='No Request Received for Tutor.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Tutor does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Request Detail -------------- */

/* --------- Session Start -------------- */
public function sessionstart(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$student_id = !empty($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($student_id) && !empty($student_id)) && (isset($request_id) && !empty($request_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$request_check = DB::table('assign_requests')->where('user_id',$student_id)
							->where('tutor_id',$user_id)->where('request_id',$request_id)->first();
			if(count($request_check)){
				if($request_check->status==1){
					$session_check = DB::table('request_sessions')->where('user_id',$student_id)
					->where('tutor_id',$user_id)->where('request_id',$request_id)->first();
					if(count($session_check)>0){
						$resultArray['status']='0';
						$resultArray['message']='Session has started already.';
						return json_encode($resultArray);
					}
					else{
						$update_arr= array('request_id' => $request_id ,'user_id'=>$student_id,'tutor_id'=>$user_id,'start_time'=>Date('Y-m-d H:i:s'),'duration'=>0, 'session_status'=>0);
						$session_id = DB::table('request_sessions')->insertGetId($update_arr);
						$request_update=array('status'=>1);
						DB::table('assign_requests')->where('id',$request_id)->update($request_update);
						$resultArray['status']='1';
						$resultArray['message']='Session has Started.';
						$resultArray['Data']['session_id']=$session_id;
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					if($request_check->status==0){
						$resultArray['message']='Request is inactive.';
					}
					elseif($request_check->status==2){
						$resultArray['message']='Request is pending.';
					}
					elseif($request_check->status==3){
						$resultArray['message']='Request is completed.';
					}
					else{
						$resultArray['message']='Invalid data.';
					}
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid Request';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Session Start -------------- */
/* --------- Session update -------------- */
public function sessionupdate(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_id = !empty($_REQUEST['session_id']) ? $_REQUEST['session_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$duration = isset($_REQUEST['duration']) ? $_REQUEST['duration'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($session_id) && !empty($session_id)) && (isset($request_id) && !empty($request_id))  && (isset($duration) && !empty($duration)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$session_check = DB::table('request_sessions')->where('id',$session_id)
							->where('request_id',$request_id)->first();
			if(count($session_check)>0){
				if($session_check->session_status==0){
					$new_duration = $session_check->duration + $duration;
					$update_arr= array('duration' => $new_duration);
					DB::table('request_sessions')->where('id',$session_id)
								->where('request_id',$request_id)->update($update_arr);
					$resultArray['status']='1';
					$resultArray['message']='data has been updated.';
					$resultArray['Data']['session_id']=$session_id;
					$resultArray['Data']['duration']=$new_duration;
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					if($session_check->session_status==1){
						$resultArray['message']='Session has been stopped.';
					}
					elseif($session_check->session_status==2){
						$resultArray['message']='Session has been paused.';
					}
					else{
						$resultArray['message']='Invalid data.';
					}
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Session does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Session update -------------- */
/* --------- Session end -------------- */
public function sessionend(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_id = !empty($_REQUEST['session_id']) ? $_REQUEST['session_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$duration = isset($_REQUEST['duration']) ? $_REQUEST['duration'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($session_id) && !empty($session_id)) && (isset($request_id) && !empty($request_id)) && (isset($duration) && !empty($duration))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$session_check = DB::table('request_sessions')->where('id',$session_id)
							->where('request_id',$request_id)->first();
			if(count($session_check)>0){
				if($session_check->session_status==1){
					$resultArray['status']='0';
					$resultArray['message']='Session already end.';
					return json_encode($resultArray);
				}
				else {
					$new_duration = $session_check->duration + $duration;
					$update_arr= array('session_status' => 1,'duration' => $new_duration);
					DB::table('request_sessions')->where('id',$session_id)
							->where('request_id',$request_id)->update($update_arr);

					$update_request= array('status' => 3);
					DB::table('assign_requests')->where('id',$request_id)->update($update_request);

					/* ---------------- amount ------------ */
					$totaltimesec = $session_check->duration; 
					$site_settings = DB::table('site_settings')->where('var_key','fees')->first();
					if(count($site_settings)>0){
						$fixamount = $site_settings->var_value;
					}
					else{
						$fixamount = 1;
					}
					$totaltime =  gmdate("H:i:s", $totaltimesec);
					$totaltimehour = date('H', strtotime($totaltime));
					$totaltimeminutes = date('i', strtotime($totaltime));
					$totaltimeseconds = date('s', strtotime($totaltime));
					if($totaltimeminutes>=30){
						$roundedtime = $totaltimehour+1;
					}
					else{
						$roundedtime = $totaltimehour;
					}
					$totalamount = $roundedtime*$fixamount;
					/* ---------------- amount ------------ */
							
					/*-- request status changed assign_requests -----*/
					$req_arr= array('status' => 3);
					DB::table('assign_requests')->where('user_id',$user_id)
							->where('request_id',$request_id)->update($req_arr);
					/*-- request status changed assign_requests -----*/
					$resultArray['status']='1';
					$resultArray['message']='Session is complete.';
					$resultArray['Data']['session_id']=$session_id;
					$resultArray['Data']['request_id']=$request_id;
					$resultArray['Data']['totalamount']=$totalamount;
					$resultArray['Data']['session_status']=1;
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Session Request does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/* --------- Session end -------------- */
/* --------- Request Support -------------- */
public function requestsupport(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$content = !empty($_REQUEST['content']) ? $_REQUEST['content'] : "";

	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($content) && !empty($content)) && (isset($request_id) && !empty($request_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$request_check = DB::table('requests')->where('id',$request_id)->first();
			if(count($request_check)>0){
				$update_arr= array('user_id' => $user_id ,'request_id'=>$request_id,'support_type'=>'Request','content'=>$content);	
				$support_id = DB::table('supports')->insertGetId($update_arr);
				$resultArray['status']='1';
				$resultArray['message']='Support request has been accepted.';
				$resultArray['Data']['user_id']=$user_id;
				$resultArray['Data']['support_id']=$support_id;
				return $resultArray;
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='Invalid Request id.';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Request Support -------------- */

/* --------- User Support -------------- */

public function usersupport(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$content = !empty($_REQUEST['content']) ? $_REQUEST['content'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($content) && !empty($content)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
				$update_arr= array('user_id' => $user_id ,'content'=>$content);	
				$support_id = DB::table('supports')->insertGetId($update_arr);
				$resultArray['status']='1';
				$resultArray['message']='User support request has been accepted.';
				$resultArray['Data']['user_id']=$user_id;
				$resultArray['Data']['support_id']=$support_id;
				return $resultArray;
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- User Support -------------- */


/* --------- Notifications -------------- */
public function notifications(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$notification_check = DB::table('notifications')->where('user_id',$user_id)
							->get();
			if(count($notification_check)>0){
				$i=0;
				$result = array();
				foreach($notification_check as $nc){
					$result[$i]['user_id']= !empty($nc->user_id)? $nc->user_id : "";
					$result[$i]['request_id']= !empty($nc->job_id)? $nc->job_id : "";
					$result[$i]['msg']= !empty($nc->msg)? $nc->msg : "";
					$result[$i]['date']= !empty($nc->updated_at)? $nc->updated_at : "";
					++$i;
				}
				$resultArray['status']='1';
				$resultArray['message']='Success.';
				$resultArray['Data']=$result;
				return $resultArray;
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No Notifications are there for the user.';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Notifications -------------- */

/* --------- Schedule -------------- */
public function schedule(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	//$user_type = !empty($_REQUEST['user_type']) ? $_REQUEST['user_type'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			
			$request_check = DB::table('users')->where('id',$user_id)->first();
			if($request_check->usertype=='Student'){
				$utype=1;
				$results = 	DB::table('requests')
								->join('assign_requests', function($join)
									{
										$join->on('requests.user_id', '=', 'assign_requests.user_id');
										$join->on('requests.id', '=', 'assign_requests.request_id');
									}
								)
								->where('assign_requests.status', '=', 1)
								->where('requests.request_date', '>=', date("Y-m-d H:i:s"))
								->get(); 
				if(count($results)>0){
					$i=0;
					$resultsarray = array();
					foreach($results as $rs){
						$resultsarray[$i]['tutor_id']= !empty($rs->tutor_id)?$rs->tutor_id:"";
						// show tutor detail 
						if(!empty($rs->tutor_id)){
							$user_detail_sc= DB::table('users')->where('id',$rs->tutor_id)->first();
							$resultsarray[$i]['tutor_name'] = !empty($user_detail_sc->name)?$user_detail_sc->name:"";
							$resultsarray[$i]['tutor_email'] = !empty($user_detail_sc->email)?$user_detail_sc->email:"";
							$resultsarray[$i]['tutor_pic'] = !empty($user_detail_sc->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail_sc->pic:"";
						}
						// show tutor detail 
						$resultsarray[$i]['request_id']= !empty($rs->request_id)?$rs->request_id:"";
						$resultsarray[$i]['request_date']= !empty($rs->request_date)?date('Y-m-d H:i:s',strtotime($rs->request_date)):"";
						$resultsarray[$i]['duration']= !empty($rs->duration)?$rs->duration:"";
						$resultsarray[$i]['location']= !empty($rs->location)?$rs->location:"";
						$resultsarray[$i]['latitude']= !empty($rs->latitude)?$rs->latitude:"";
						$resultsarray[$i]['longitude']= !empty($rs->longitude)?$rs->longitude:"";
						//$resultsarray[$i]['subject_id']= !empty($rs->subject_id)?$rs->subject_id:"";


						$subject_idarray = explode(',',$rs->subject_id);
						$subjects=array();
						foreach($subject_idarray as $k=>$subject){ 
							$resultsubject = DB::table('subject')->where('id',$subject)->first();
							if(count($resultsubject)>0){
								//$subjects[$k][$subject]=$resultsubject->title;
								$subjects[$k]['subject_id']=$subject;
								$subjects[$k]['subject_name']=$resultsubject->title;
							}
								
						} 
						$resultsarray[$i]['subjects']=$subjects;



						$resultsarray[$i]['status']= !empty($rs->status)?$rs->status:"";
						++$i;
					}
					$resultArray['status']='1';
					$resultArray['message']='schedule.';
					$resultArray['usertype']=$utype;
					$resultArray['Data']=$resultsarray;
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No schedule for '.$request_check->usertype;
					return json_encode($resultArray);
				}
						
			}
			else{
				
					$utype=2;
					$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
									}
								)
								->where('assign_requests.status', '=', 1)
								->where('requests.request_date', '>=', date("Y-m-d H:i:s"))
								->where('assign_requests.tutor_id', '=', $user_id)
								->get(); 
					if(count($results)>0){
						$i=0;
						//print_r($results);exit;
						$resultsarray = array();
						foreach($results as $rs){
							$resultsarray[$i]['user_id']= !empty($rs->tutor_id)?$rs->user_id:"";
							// show user detail 
							if(!empty($rs->tutor_id)){
								$user_detail_sc= DB::table('users')->where('id',$rs->user_id)->first();
								$resultsarray[$i]['user_name'] = !empty($user_detail_sc->name)?$user_detail_sc->name:"";
								$resultsarray[$i]['user_email'] = !empty($user_detail_sc->email)?$user_detail_sc->email:"";
								$resultsarray[$i]['user_pic'] = !empty($user_detail_sc->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail_sc->pic:"";
							}
							// show user detail 
							$resultsarray[$i]['request_id']= !empty($rs->request_id)?$rs->request_id:"";
							$resultsarray[$i]['request_date']= !empty($rs->request_date)?date('Y-m-d H:i:s',strtotime($rs->request_date)):"";
							$resultsarray[$i]['duration']= !empty($rs->duration)?$rs->duration:"";
							$resultsarray[$i]['location']= !empty($rs->location)?$rs->location:"";
							$resultsarray[$i]['latitude']= !empty($rs->latitude)?$rs->latitude:"";
							$resultsarray[$i]['longitude']= !empty($rs->longitude)?$rs->longitude:"";
							//$resultsarray[$i]['subject_id']= !empty($rs->subject_id)?$rs->subject_id:"";



							$subject_idarray = explode(',',$rs->subject_id);
							$subjects=array();
							foreach($subject_idarray as $k=>$subject){ 
								$resultsubject = DB::table('subject')->where('id',$subject)->first();
								if(count($resultsubject)>0){
									$subjects[$k]['subject_id']=$subject;
									$subjects[$k]['subject_name']=$resultsubject->title;
								}
									
							} 
							$resultsarray[$i]['subjects']=$subjects;


							$resultsarray[$i]['status']= !empty($rs->status)?$rs->status:"";
							++$i;
						}
						$resultArray['status']='1';
						$resultArray['message']='schedule.';
						$resultArray['usertype']=$utype;
						$resultArray['Data']=$resultsarray;
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='No schedule for '.$request_check->usertype;
						return json_encode($resultArray);
					}
				

			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Schedule -------------- */

/* --------- Add Favourites -------------- */
public function addfavourites(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$tutor_id = !empty($_REQUEST['tutor_id']) ? $_REQUEST['tutor_id'] : "";
	$status = !empty($_REQUEST['status']) ? $_REQUEST['status'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($tutor_id) && !empty($tutor_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$user_check= DB::table('users')->where('id',$user_id)->where('usertype','Student')->first();
			if(count($user_check)>0){
				$tutor_check= DB::table('users')->where('id',$tutor_id)->where('usertype','Tutor')->first();
				if(count($tutor_check)>0){
					$chk_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$tutor_id)->first(); 
					if(count($chk_fav)>0)
					{
						if(isset($_REQUEST['status']) && $_REQUEST['status']!="")
						{	
							$req_arr= array('status' => $_REQUEST['status']);
							DB::table('user_favourites')->where('user_id',$user_id)
							->where('tutor_id',$tutor_id)->update($req_arr);
							$resultArray['status']='1';
							if($_REQUEST['status']==0)
								$resultArray['message']='Tutor is removed from the favourite list.';
							elseif($_REQUEST['status']==1)
								$resultArray['message']='Tutor is added the favourite list.';
							else
								$resultArray['message']='Invalid Status.';
							return json_encode($resultArray);
						}
						else
						{
							if($chk_fav->status==0){
								$req_arr= array('status' => 1);
								DB::table('user_favourites')->where('user_id',$user_id)
								->where('tutor_id',$tutor_id)->update($req_arr);
								$resultArray['status']='1';
								$resultArray['message']='Tutor is added to the favourite list.';
								return json_encode($resultArray);
							}
							else{
								$resultArray['status']='0';
								$resultArray['message']='Tutor is already in favourites list.';
								return json_encode($resultArray);
							}
						}
					}
					else
					{
						$update_arr= array('user_id' => $user_id ,'tutor_id'=>$tutor_id,'status'=>1);
						DB::table('user_favourites')->insert($update_arr);
						$resultArray['status']='1';
						$resultArray['message']='Tutor is added to the favourite list.';
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid Tutor.';
					return json_encode($resultArray);
				}

			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid User';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/* --------- Add Favourites -------------- */

/* --------- Add unFavourites -------------- */
public function addunfavourites(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$tutor_id = !empty($_REQUEST['tutor_id']) ? $_REQUEST['tutor_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($tutor_id) && !empty($tutor_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$user_check= DB::table('users')->where('id',$user_id)->where('usertype','Student')->first();
			if(count($user_check)>0){
				$tutor_check= DB::table('users')->where('id',$tutor_id)->where('usertype','Tutor')->first();
				if(count($tutor_check)>0){
					$chk_fav = DB::table('user_favourites')->where('user_id',$user_id)->where('tutor_id',$tutor_id)->first(); 
					if(count($chk_fav)>0)
					{
						if($chk_fav->status!=0)
						{	
							$req_arr= array('status' => 0);
							DB::table('user_favourites')->where('user_id',$user_id)
							->where('tutor_id',$tutor_id)->update($req_arr);
							$resultArray['status']='1';
							$resultArray['message']='Tutor is added to the Unfavourite list.';
							return json_encode($resultArray);
						}
						else
						{
							$resultArray['status']='0';
							$resultArray['message']='Tutor is already in the Unfavourite list.';
							return json_encode($resultArray);
						}
					}
					else
					{
						$update_arr= array('user_id' => $user_id ,'tutor_id'=>$tutor_id,'status'=>0);
						DB::table('user_favourites')->insert($update_arr);
						$resultArray['status']='1';
						$resultArray['message']='Tutor is added to the Unfavourite list.';
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid Tutor.';
					return json_encode($resultArray);
				}

			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid User';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/* --------- Add unFavourites -------------- */


/* --------- show Favourites -------------- */
public function showfavourites(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$user_check= DB::table('users')->where('id',$user_id)->where('usertype','Student')->first();
			if(count($user_check)>0){
				$fav_coll= DB::table('user_favourites')->where('user_id',$user_id)->where('status',1)->get();
				if(count($fav_coll)>0){
					$i=0;
					foreach($fav_coll as $fc){
						//echo $fc->tutor_id;exit;
						$resultsarray[$i]['tutor_id'] = $fc->tutor_id;
						$user_detail= DB::table('users')->where('id',$fc->tutor_id)->where('status',1)->first();
						$resultsarray[$i]['name'] = !empty($user_detail->name)?$user_detail->name:"";
						$resultsarray[$i]['username'] = !empty($user_detail->username)?$user_detail->username:"";
						$resultsarray[$i]['email'] = !empty($user_detail->email)?$user_detail->email:"";
						$resultsarray[$i]['pic'] = !empty($user_detail->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail->pic:"";
						$resultsarray[$i]['usertype'] = !empty($user_detail->usertype)?$user_detail->usertype:"";
						$resultsarray[$i]['dob'] = !empty($user_detail->dob)?$user_detail->dob:"";
						$resultsarray[$i]['phone1'] = !empty($user_detail->phone1)?$user_detail->phone1:"";
						$resultsarray[$i]['address'] = !empty($user_detail->address)?$user_detail->address:"";
						$resultsarray[$i]['city'] = !empty($user_detail->city)?$user_detail->city:"";
						$resultsarray[$i]['state'] = !empty($user_detail->state)?$user_detail->state:"";
						$resultsarray[$i]['country'] = !empty($user_detail->country)?$user_detail->country:"";

						//
						$subject_idarray = explode(',',$user_detail->subject_id);
						$subjects=array();
						foreach($subject_idarray as $k=>$subject)
						{ 
							$resultsubject = DB::table('subject')->where('id',$subject)->first();
							if(count($resultsubject)>0){
								$subjects[$k]['subject_name']=$resultsubject->title;
								$subjects[$k]['subject_id']=$subject;
							}
						} 
						$resultsarray[$i]['subjects']=$subjects;
						//
						++$i;

					}
					$resultArray['status']='1';
					$resultArray['message']='Favourite Tutor list.';
					$resultArray['Data']=$resultsarray;
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No favourite tutor in the list';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid User';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- show Favourites -------------- */



/* --------- studentreview -------------- */
public function studentreview(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$rate = !empty($_REQUEST['rate']) ? $_REQUEST['rate'] : "";
	$comment = !empty($_REQUEST['comment']) ? $_REQUEST['comment'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id)) && (isset($comment) && !empty($comment)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$user_check= DB::table('assign_requests')->where('user_id',$user_id)->where('request_id',$request_id)->first();
			if(count($user_check)>0){
				$finalrate=0;
				if(!empty($rate)){
					$finalrate=$rate;
				}
				$reviewinsert = DB::table('student_review')->insertGetId([
										'user_id' =>$user_id, 
										'request_id' => $request_id,
										'tutor_id' => $user_check->tutor_id,
										'comment' => $comment,
										'rate' => $finalrate
										]);
				//notification for user
					$abc = DB::table('notifications')->insert([
										'user_id' =>$user_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>'Your request is completed.'
										]);
					//notification for user

				$resultArray['status']='1';
				$resultArray['message']='Thank you for the review.';
				$resultArray['review_id']=$reviewinsert;
				return json_encode($resultArray);
			}
			else
			{
				$resultArray['status']='0';
				$resultArray['message']='invalid request.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- studentreview -------------- */

/* --------- tutorshowreview -------------- */
public function tutorshowreview(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else
		{
			$user_check= DB::table('users')->where('id',$user_id)->where('usertype','Tutor')->first();
			if(count($user_check)>0)
			{
				$review_check= DB::table('student_review')->where('tutor_id',$user_id)->get();
				if(count($review_check)>0)
				{
					$i=0;
					foreach ($review_check as $rc) {
						$resultsarray[$i]['user_id'] =  !empty($rc->user_id)?$rc->user_id:"";
						//user detail
						if(!empty($rc->user_id)){
							$user_detail_sc= DB::table('users')->where('id',$rc->user_id)->first();
							$resultsarray[$i]['user_name'] = !empty($user_detail_sc->name)?$user_detail_sc->name:"";
							$resultsarray[$i]['user_email'] = !empty($user_detail_sc->email)?$user_detail_sc->email:"";
							$resultsarray[$i]['user_pic'] = !empty($user_detail_sc->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail_sc->pic:"";
						}
						//user detail
						$resultsarray[$i]['request_id']= !empty($rc->request_id)?$rc->request_id:"";
						$resultsarray[$i]['comment'] = !empty($rc->comment)?$rc->comment:"";
						++$i;
					}
					$resultArray['status']='1';
					$resultArray['message']='Success! Reviews by the user.';
					$resultArray['Data']=$resultsarray;
					return json_encode($resultArray);
				}
				else
				{
					$resultArray['status']='0';
					$resultArray['message']='No review for the tutor.';
					return json_encode($resultArray);
				}
			}
			else
			{
				$resultArray['status']='0';
				$resultArray['message']='Invalid user.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- show Favourites -------------- */





/* --------- Request Tutor -------------- */
public function tutorjobs(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$status = 2;
	if((isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$student_req = DB::table('users')->where('id',$user_id)->where('usertype','Tutor')->first();
			if(count($student_req)>0){
				$tut_req = DB::table('assign_requests')->where('tutor_id',$user_id)->first();
				if(count($tut_req)>0){
					$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
										$join->on('assign_requests.user_id', '=', 'requests.user_id');
									}
								)
								->where('assign_requests.tutor_id', '=', $user_id)
								->where('assign_requests.status', '=', $status)
								->get(); 
					if(count($results)>0){
						$i=0;
						$result_store_array=array();
						foreach($results as $result){ 
							$result_store_array[$i]['user_id'] = $result->user_id;
							//user image 
							$user_img = DB::table('users')->where('id',$result->user_id)->first();
							$result_store_array[$i]['user_pic']=!empty($user_img->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_img->pic:"";
							$result_store_array[$i]['user_name']=!empty($user_img->name)?$user_img->name:"";
							//user image 
							$result_store_array[$i]['location'] = $result->location;
							$result_store_array[$i]['duration'] = $result->duration;
							$result_store_array[$i]['request_id'] = $result->request_id;
							$result_store_array[$i]['request_date'] = $result->request_date;
							$result_store_array[$i]['status'] = $result->status;
							$result_store_array[$i]['latitude'] = !empty($result->latitude)?$result->latitude:"";
							$result_store_array[$i]['longitude'] = !empty($result->longitude)?$result->longitude:"";
							//$result_store_array[$i]['subject_id'] = $result->subject_id;
							$subject_idarray = explode(',',$result->subject_id);
							$subjects=array();
							foreach($subject_idarray as $k=>$subject){ 
								$resultsubject = DB::table('subject')->where('id',$subject)->first();
								if(count($resultsubject)>0){
									//$subjects[$k][$subject]=$resultsubject->title;
									$subjects[$k]['subject_id']=$subject;
									$subjects[$k]['subject_name']=$resultsubject->title;
								}
									
							} 
							$result_store_array[$i]['subjects']=$subjects;
							++$i;
						}
						$resultArray['status']='1';
						$resultArray['message']='Success.';
						$resultArray['Data']['history']=$result_store_array;
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='No request is completed.';
						return json_encode($resultArray);
					}
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='No Request Received for Tutor.';
				return json_encode($resultArray);
			}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Tutor does not exist.';
			return json_encode($resultArray);
		}
	}
}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Request Detail -------------- */



/* --------- Session complete amount -------------- */
public function sessionamount(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$session_id = !empty($_REQUEST['session_id']) ? $_REQUEST['session_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($session_id) && !empty($session_id)) && (isset($request_id) && !empty($request_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{ 
			$session_check = DB::table('request_sessions')->where('id',$session_id)
							->where('request_id',$request_id)->first();
			if(count($session_check)>0){  
				if($session_check->session_status==1){
					$totaltimesec = $session_check->duration; 
					$site_settings = DB::table('site_settings')->where('var_key','fees')->first();
					if(count($site_settings)>0){
						$fixamount = $site_settings->var_value;
					}
					else{
						$fixamount = 1;
					}
					$totaltime =  gmdate("H:i:s", $totaltimesec);
					$totaltimehour = date('H', strtotime($totaltime));
					$totaltimeminutes = date('i', strtotime($totaltime));
					$totaltimeseconds = date('s', strtotime($totaltime));
					if($totaltimeminutes>=30){	
						$roundedtime = $totaltimehour+1;
					}
					else{
						$roundedtime = $totaltimehour;
					}
					$totalamount = $roundedtime*$fixamount;
					/* -------------- user request detail ----------------*/
					$user_details_request=DB::table('requests')
											->where('user_id',$session_check->user_id)
											->where('id',$session_check->request_id)
											->first();
					if(count($user_details_request)>0){
						$resultArray['Data']['location'] = !empty($user_details_request->location)?$user_details_request->location:"";
						$resultArray['Data']['booking_type'] = !empty($user_details_request->booking_type)?$user_details_request->booking_type:"";
						$resultArray['Data']['request_date'] = !empty($user_details_request->request_date)?$user_details_request->request_date:"";
						$resultArray['Data']['request_id'] = !empty($user_details_request->id)?$user_details_request->id:"";
						$subject_idarray = explode(',',$user_details_request->subject_id);
						$subjects=array();
						foreach($subject_idarray as $k=>$subject){ 
							$resultsubject = DB::table('subject')->where('id',$subject)->first();
							if(count($resultsubject)>0){
								//$subjects[$k][$subject]=$resultsubject->title;
								$subjects[$k]['subject_id']=$subject;
								$subjects[$k]['subject_name']=$resultsubject->title;
							}
								
						} 
						$resultArray['Data']['subjects']=$subjects;
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='Invalid Request.';
						return json_encode($resultArray);
					}
					/* -------------- user request detail ----------------*/
					/* -------------- user detail ----------------*/
					$user_request_details=DB::table('users')
											->where('id',$user_id)
											->where('usertype','Student')
											->first();
					if(count($user_request_details)>0){
						$resultArray['Data']['name'] = !empty($user_request_details->name)?$user_request_details->name:"";
						$resultArray['Data']['email'] = !empty($user_request_details->email)?$user_request_details->email:"";
						$resultArray['Data']['pic'] = !empty($user_request_details->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_request_details->pic:"";
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='Invalid User.';
						return json_encode($resultArray);
					}
					/* -------------- user detail ----------------*/

					$resultArray['status']='1';
					$resultArray['message']='Session complete.';
					$resultArray['Data']['totaltime']=$totaltime;
					$resultArray['Data']['totalamount']=$totalamount;
					return json_encode($resultArray);
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='Session is running.';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Session Request does not exist.';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- Session complete amount -------------- */


/*------ fly tutor auto assign ------------ */
public function tutorautoassign(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key  = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id      = !empty($_REQUEST['id']) ? $_REQUEST['id'] : "";
	$subject_id   = !empty($_REQUEST['subject_id']) ? $_REQUEST['subject_id']: "";
	$location     = !empty($_REQUEST['location']) ? $_REQUEST['location'] : "";
	$duration     = !empty($_REQUEST['duration']) ? $_REQUEST['duration'] : "";
	$lat          = !empty($_REQUEST['lat']) ? $_REQUEST['lat'] : "";
	$long         = !empty($_REQUEST['long']) ? $_REQUEST['long'] : "";
	$date         = !empty($_REQUEST['date']) ? $_REQUEST['date'] : "";
	//$booking_type = !empty($_REQUEST['booking_type']) ? $_REQUEST['booking_type'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($subject_id) && !empty($subject_id)) && (isset($location) && !empty($location)) && (isset($duration) && !empty($duration)) && (isset($session_key) && !empty($session_key)) && (isset($date) && !empty($date)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else
		{
			$subject_store = $subject_id[0];
			$subject_arr = explode(',',$subject_id[0]);
			$subject_count = 0;
			foreach($subject_arr as $k=>$subject){
				$chkuserSubject = DB::table('subject')->where('id',$subject)->first();
				if(count($chkuserSubject)!=0)
					$subject_count = ++$subject_count;
			}
			if($subject_count!=0){
				$chkuserdata = DB::table('users')->where('usertype','Tutor')->get();
				$userdetailarray = array();
				$sub_store = array();
				if(count($chkuserdata)>0){
					foreach($chkuserdata as $key=>$users){ 
						foreach($subject_arr as $subjectarray){ 
							$userdetailarray= explode(',',$users->subject_id); 
							if(in_array($subjectarray, $userdetailarray)){ 
								$sub_store[]= !empty($users->id)?$users->id:"";
							}
						}
					}
					//subject with tutor
					if(count($sub_store)>0){
						$book_type = 'fly';
						$parsedtime = date_parse($duration);
						$timeseconds = $parsedtime['hour'] * 3600 + $parsedtime['minute'] * 60 + $parsedtime['second'];
						$user = DB::table('requests')->insertGetId([
										'user_id' =>$user_id, 
										'subject_id' => $subject_store,
										'location' => $location,
										'latitude' => $lat,
										'longitude' => $long,
										'duration' => $timeseconds,
										'booking_type' => $book_type,
										'request_date' => date('Y-m-d H:i:s',strtotime($date))
										]);
						$subject_store = array();
						$userdetailarray = array();
						$sub_result = array_unique($sub_store);
						$sub_result = array_values($sub_result);
						$randomtutor = array_rand($sub_result);
						$randomtutor = $sub_result[$randomtutor];

							//random tutor
							$users = DB::table('users')->where('id',$randomtutor)->first();

							$userdetailarray['tutorinfo']['id']= !empty($users->id)?$users->id:"";
							$userdetailarray['tutorinfo']['name']=!empty($users->name)?$users->name:"";
							$userdetailarray['tutorinfo']['username'] =  !empty($users->username)?$users->username:"";
							$userdetailarray['tutorinfo']['email']=!empty($users->email)?$users->email:"";
							$userdetailarray['tutorinfo']['pic'] =   !empty($users->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$users->pic:"";
							$utype=2;
							if($users->usertype=="Student"){
								$utype=1;
							}

							$userdetailarray['tutorinfo']['usertype'] =   $utype;
							$userdetailarray['tutorinfo']['dob'] =   !empty($users->dob)?$users->dob:"";
							$userdetailarray['tutorinfo']['address'] =   !empty($users->address)?$users->address:"";
							$userdetailarray['tutorinfo']['city'] =!empty($users->city)?$users->city:"";
							$userdetailarray['tutorinfo']['state'] =   !empty($users->state)?$users->state:"";
							$userdetailarray['tutorinfo']['country'] =   !empty($users->country)?$users->country:"";
							$userdetailarray['tutorinfo']['phone1'] =   !empty($users->phone1)?$users->phone1:"";
							$userdetailarray['tutorinfo']['phone2'] =   !empty($users->phone2)?$users->phone2:"";
							$userdetailarray['tutorinfo']['phone3'] =   !empty($users->phone3)?$users->phone3:"";
							$userdetailarray['tutorinfo']['zip'] =   !empty($users->zip)?$users->zip:"";
							$userdetailarray['tutorinfo']['confirmed'] =   !empty($users->confirmed)?$users->confirmed:"";
							$userdetailarray['tutorinfo']['created_at'] =   !empty($users->created_at)?$users->created_at:"";
							$userdetailarray['tutorinfo']['updated_at'] =   !empty($users->updated_at)?$users->updated_at:"";
							$tutor_set = DB::table('site_settings')->where('var_key','fees')->first();
							if(count($tutor_set)>0){
								$userdetailarray[$tutor_set->var_key]=$tutor_set->var_value;
							} 
							$subject_store = explode(',',$users->subject_id);
							$subjects='';
							foreach($subject_store as $key=>$sub)
							{
								$subdata = DB::table('subject')->where('id',$sub)->first();
								$subjects[$key]['id']=$subdata->id;
								$subjects[$key]['subject']=$subdata->title;
							}
							$userdetailarray['tutorinfo']['subjects']=$subjects;
							//random tutor
						//request assign
						$req = DB::table('assign_requests')->insert([
										'tutor_id' =>$userdetailarray['tutorinfo']['id'],
										'user_id' =>$user_id,
										'request_id' => $user,
										'status' => 2
										]);

						//request assign
						//notification for user
						$abc = DB::table('notifications')->insert([
											'user_id' =>$user_id,
											'job_id' =>$user,
											'type' =>'Request',
											'status' => 1,
											'msg'=>'your request is pending',
											'created_at'=>date('Y-m-d H:i:s'),
											'updated_at'=>date('Y-m-d H:i:s')
											]);
						//notification for user
						//notification for tutor
						$xyz = DB::table('notifications')->insert([
											'user_id' =>$userdetailarray['tutorinfo']['id'],
											'job_id' =>$user,
											'type' =>'Request',
											'status' => 1,
											'msg'=>'You have a new request.',
											'created_at'=>date('Y-m-d H:i:s'),
											'updated_at'=>date('Y-m-d H:i:s')
											]);

						//notification for tutor
						$resultArray['status']='1';
						$resultArray['message']='Your request is pending for response.';
						$userdetailarray['booking_type'] = !empty($book_type)?$book_type:"";
						$resultArray['request_id'] =  	!empty($user)?$user:"";
						$resultArray['duration'] =  	!empty($timeseconds)?$timeseconds:"";
						$resultArray['Data']['tutor']=$userdetailarray;	
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='this subject no tutor avaliable.';
						return json_encode($resultArray);
					}
				}
				else {
					$resultArray['status']='0';
					$resultArray['message']='No Tutor.';
					return json_encode($resultArray);
				}
			}
			else {
				$resultArray['status']='0';
				$resultArray['message']='Invalid subject.';
				return json_encode($resultArray);
			}
		}
	}
	else
	{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*------ fly tutor auto assign ------------ */
public function flycancelrequest(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$tutor_id = !empty($_REQUEST['tutor_id']) ? $_REQUEST['tutor_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$reason = !empty($_REQUEST['reason']) ? $_REQUEST['reason'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($tutor_id) && !empty($tutor_id)) && (isset($request_id) && !empty($request_id)) && (isset($reason) && !empty($reason)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{ 
			$request_check = DB::table('requests')->where('user_id',$user_id)
							->where('id',$request_id)->first();
			if(count($request_check)>0){
				$assignrequest_check = DB::table('assign_requests')->where('user_id',$user_id)
							->where('tutor_id',$tutor_id)->where('request_id',$request_id)->first();
				if(count($assignrequest_check)>0){
					$update_arr= array('status' => 0,'cancel_reason'=> $reason);
					DB::table('assign_requests')->where('tutor_id',$tutor_id)->where('user_id',$user_id)->where('request_id',$request_id)->update($update_arr);
					//notification for user
					$abc = DB::table('notifications')->insert([
										'user_id' =>$user_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>'You have canceled the request',
										'created_at'=>date('Y-m-d H:i:s'),
										'updated_at'=>date('Y-m-d H:i:s')
										]);
					//notification for user
					$resultArray['status']='1';
					$resultArray['message']='your request has been canceled.';
					return json_encode($resultArray);

				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No request assign to a tutor';
					return json_encode($resultArray);
				}		
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No request found';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*------ fly tutor cancel request ------------ */
/*------ fly request list ------------ */
public function requestlist(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$req_type = !empty($_REQUEST['req_type']) ? $_REQUEST['req_type'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($req_type) && !empty($req_type))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$request_check = DB::table('requests')->where('booking_type',$req_type)->get();
			if(count($request_check)>0){
				$results = 	DB::table('requests')
								->join('assign_requests', function($join)
									{
										$join->on('requests.id', '=', 'assign_requests.request_id');
										$join->on('requests.user_id', '=', 'assign_requests.user_id');
									}
								)
								->where('assign_requests.tutor_id', '=', $user_id)
								->where('assign_requests.status', '=', 2)
								->where('booking_type', '=', $req_type)
								->get(); 
				if(count($results)>0){
					$i=0;
					foreach($results as $rslt){
						$resultsarray[$i]['user_id']=!empty($rslt->user_id)?$rslt->user_id:"";
						//user detail
						$userdetail = DB::table('users')->where('id',$rslt->user_id)->first();
						if(count($userdetail)>0){
							$resultsarray[$i]['user']['id']=!empty($userdetail->id)?$userdetail->id:"";
							$resultsarray[$i]['user']['name']=!empty($userdetail->name)?$userdetail->name:"";
							$resultsarray[$i]['user']['username']=!empty($userdetail->username)?$userdetail->username:"";
							$resultsarray[$i]['user']['email']=!empty($userdetail->email)?$userdetail->email:"";
							$resultsarray[$i]['user']['dob']=!empty($userdetail->dob)?$userdetail->dob:"";
							$resultsarray[$i]['user']['pic']=!empty($userdetail->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$userdetail->pic:"";
							$resultsarray[$i]['user']['phone1']=!empty($userdetail->phone1)?$userdetail->phone1:"";
							$resultsarray[$i]['user']['phone2']=!empty($userdetail->phone2)?$userdetail->phone2:"";
						}
						//user detail
						$resultsarray[$i]['latitude']=!empty($rslt->latitude)?$rslt->latitude:"";
						$resultsarray[$i]['longitude']=!empty($rslt->longitude)?$rslt->longitude:"";
						$resultsarray[$i]['location']=!empty($rslt->location)?$rslt->location:"";
						$resultsarray[$i]['duration']=!empty($rslt->duration)?$rslt->duration:"";
						$resultsarray[$i]['booking_type']=!empty($rslt->booking_type)?$rslt->booking_type:"";
						$resultsarray[$i]['request_date']=!empty($rslt->request_date)?$rslt->request_date:"";
						$resultsarray[$i]['request_id']=!empty($rslt->request_id)?$rslt->request_id:"";
						$resultsarray[$i]['created_at']=!empty($rslt->created_at)?$rslt->created_at:"";
						$resultsarray[$i]['updated_at']=!empty($rslt->updated_at)?$rslt->updated_at:"";
						$subject_idarray = explode(',',$rslt->subject_id);
						$subjects=array();
						foreach($subject_idarray as $k=>$subject){ 
							$resultsubject = DB::table('subject')->where('id',$subject)->first();
							if(count($resultsubject)>0){
								//$subjects[$k][$subject]=$resultsubject->title;
								$subjects[$k]['subject_id']=$subject;
								$subjects[$k]['subject_name']=$resultsubject->title;
							}
								
						} 
						$resultsarray[$i]['subjects']=$subjects;
						++$i;
					}
					$resultArray['status']='1';
					$resultArray['message']='Succcess';
					$resultArray['Data']['request_list']=$resultsarray;
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No request found';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No request found';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*------ fly request list------------ */
/*------ request cancel by tutor ------------ */
public function cancelreqtutor(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$student_id = !empty($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$reason = !empty($_REQUEST['reason']) ? $_REQUEST['reason'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($student_id) && !empty($student_id)) && (isset($request_id) && !empty($request_id)) && (isset($reason) && !empty($reason)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$request_check = DB::table('requests')->where('id',$request_id)->first();
			if(count($request_check)>0){
				$assignrequest_check = DB::table('assign_requests')->where('tutor_id',$user_id)
							->where('user_id',$student_id)->where('request_id',$request_id)->first();
				if(count($assignrequest_check)>0){
					$update_arr= array('status' => 4,'cancel_reason'=> $reason);
					DB::table('assign_requests')->where('tutor_id',$user_id)->where('user_id',$student_id)->where('request_id',$request_id)->update($update_arr);
					//notification for user
					$abc = DB::table('notifications')->insert([
										'user_id' =>$student_id,
										'job_id' =>$request_id,
										'type' =>'Request',
										'status' => 1,
										'msg'=>'Your request has been canceled by tutor.',
										'created_at'=>date('Y-m-d H:i:s'),
										'updated_at'=>date('Y-m-d H:i:s')
										]);
					//notification for user
					$resultArray['status']='1';
					$resultArray['message']='your request has been canceled.';
					return json_encode($resultArray);
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No request assign to a tutor';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No request found';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*------ request cancel by tutor ------------ */





 



/*-------- 1. checkout url---------- */
public function checkouturl()
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{ 
			$resultArray['status']='1';
			$resultArray['message']='success';
			$resultArray['Data']['url']='http://laravel.kart247.com/tutorondemand/public/api/checkout?user_id='.$user_id.'&session_keys='.$session_key;
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*--------1. checkout url---------- */
/*--------2. checkout ---------- */
public function checkout($status=null,$email=null,$message=null)
{
	$input = Input::all();
	$user_id = isset($input['user_id'])?$input['user_id']:"";
	$session_keys = isset($input['session_keys'])?$input['session_keys']:"";
	Session::put('id.session.id', $user_id);
	Session::put('id.session.session', $session_keys);
	return view('frontend.user.checkout',compact('input'));
}
/*--------2. checkout ---------- */
/*--------3. checkout post---------- */
public function checkoutpost()
{
	if(isset($_POST)){
		$input = Input::all();
		$user_id=isset($input['user_id'])?$input['user_id']:"";
		$detail = isset($input['detail'])?$input['detail']:"";
		$amount = isset($input['amount'])?$input['amount']:"";
		$name = isset($input['name'])?$input['name']:"";
		$email = isset($input['email'])?$input['email']:"";
		$phone = isset($input['phone'])?$input['phone']:"";
		$update_arr= array('cust_id'=>$user_id,'detail'=>$detail,'amount'=>$amount,'name'=>$name,'email'=>$email,'phone'=>$phone,'created_at'=>Date('Y-m-d H:i:s'));
		$orderid = DB::table('customer_order')->insertGetId($update_arr);	
		$input['orderid']=$orderid;
		Session::put('id.session.orderid', $orderid);
	}
	else{
		$input = Input::all();
	}
	return view('frontend.user.checkoutpost',compact('input'));
}
/*--------3. checkout post---------- */
/*--------4. Success---------- */
public function success()
{
	if(isset($_GET)){
		$input = Input::all();
		$status_id=isset($input['status_id'])?$input['status_id']:"";
		$transaction_id = isset($input['transaction_id'])?$input['transaction_id']:"";
		$msg = isset($input['msg'])?$input['msg']:"";
		$hash = isset($input['hash'])?$input['hash']:"";
		Session::put('id.session.msg', $msg);
	}
	return view('frontend.user.success');
}
/*--------4. Success---------- */
/* ---------5. checkout update success -------*/
public function checkoutpostsuccess($resultarray)
{

	if(isset($resultarray['order_id'])){

		$update_arr= array('status_id' => $resultarray['status'],'message' => $resultarray['message'],'transaction_id' => $resultarray['transaction_id'],'hash' => $resultarray['hash'],'updated_at' => Date('Y-m-d H:i:s'));
		DB::table('customer_order')->where('id',$resultarray['order_id'])->update($update_arr);

		$order=DB::table('customer_order')->where('id',$resultarray['order_id'])->orderBy('id', 'desc')->first();
		if(count($order)>0){
			if(empty($order->request_id)){

				$user=DB::table('users')->where('id',$order->cust_id)->first();
				if(count($user)>0){
					$creaditamount = $user->credit_amount+$order->amount;
					$update_arr= array('credit_amount' => $creaditamount );
					DB::table('users')->where('id',$order->cust_id)->update($update_arr);
				}
			}
		}
		return;
	}
	return;
}
/* ---------5. checkout update success -------*/

/*---6. checkout final info */
public function checkoutfinalinfo(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			//$request_check = DB::table('requests')->where('id',$request_id)->first();
			$orderid = Session::get('id.session.orderid');
			$userid = Session::get('id.session.id');
			$sessionkeys = Session::get('id.session.session');
			$msg = Session::get('id.session.msg');

			$resultArray['status']='1';
			$resultArray['message']='Success';
			$resultArray['Data']['orderid']=$orderid;
			$resultArray['Data']['userid']=$userid;
			$resultArray['Data']['sessionkeys']=$sessionkeys;
			$resultArray['Data']['msg']=$msg;
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*---6. checkout final info */

/*---7. creadit balance */
public function usercreditbalance(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$user_check = DB::table('users')->where('id',$user_id)->first();
			if(count($user_check)>0){
				$resultArray['status']='0';
				$resultArray['message']='Invalid parameter';
				$resultArray['Data']['credit_amount']=$user_check->credit_amount;
				return json_encode($resultArray);
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid user';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*---7. creadit balance */


public function checkouttransaction() 
{
	if(isset($_GET)){
		$input = Input::all();
		$status=isset($input['status'])?$input['status']:"";
		$msg = isset($input['msg'])?$input['msg']:"";
		$resultArray['status']='1';
		$resultArray['message']='Success';
		$resultArray['Data']['fackurl']='http://www.fackurlforwdo.com?status='.$status.'msg='.$msg;
		$resultArray['Data']['status'] =$status;
		$resultArray['Data']['msg']=$msg;
		return json_encode($resultArray);
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Falied';
		return json_encode($resultArray);
	}
}


/*------ student pay by cash ------------ */
public function stupaycash(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$amount = !empty($_REQUEST['amount']) ? $_REQUEST['amount'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id)) && (isset($amount) && !empty($amount))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$request_check = DB::table('requests')->where('id',$request_id)->first();
			if(count($request_check)>0){
				$results = 	DB::table('requests')
								->join('assign_requests', function($join)
									{
										$join->on('requests.id', '=', 'assign_requests.request_id');
										$join->on('requests.user_id', '=', 'assign_requests.user_id');
									}
								)
								->where('assign_requests.user_id', '=', $user_id)
								->where('assign_requests.status', '=', 3)
								->where('assign_requests.request_id', '=',$request_id)
								->first(); 
				if(count($results)>0){
					$udetail=DB::table('users')->where('id',$user_id)->first();
					$cust_name="";
					$cust_email="";
					$cust_phone="";
					if(count($udetail)>0){
						$cust_name=!empty($udetail->name)?$udetail->name:"";
						$cust_email=!empty($udetail->email)?$udetail->email:"";
						$cust_phone=!empty($udetail->phone1)?$udetail->phone1:"";
					}
					$update_arr= array('cust_id'=>$results->user_id,'tutor_id'=>$results->tutor_id,'request_id'=>$results->request_id,'detail'=>'cash','amount'=>$amount,'name'=>$cust_name,'email'=>$cust_email,'phone'=>$cust_phone,'status_id'=>1,'message'=>'Payment by cash','created_at'=>Date('Y-m-d H:i:s'),'updated_at'=>Date('Y-m-d H:i:s'));
					$tranc_id = DB::table('customer_order')->insertGetId($update_arr);
					if(!empty($tranc_id)){
						$resultArray['status']='1';
						$resultArray['message']='Your Transaction is successfull';
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='Your Transaction is Unsuccessfull';
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='No request entry';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No request found';
				return json_encode($resultArray);
			}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*------ student pay by cash ------------ */
/*------------------- student checkout ---------------------*/
/*-------- 1. student checkout url---------- */
public function studentcheckouturl()
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	$amount = !empty($_REQUEST['amount']) ? $_REQUEST['amount'] : "";
	if((isset($user_id) && !empty($user_id)) && (isset($session_key) && !empty($session_key)) && (isset($amount) && !empty($amount)) && (isset($request_id) && !empty($request_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{ 
			$resultArray['status']='1';
			$resultArray['message']='success';
			$resultArray['Data']['url']='http://laravel.kart247.com/tutorondemand/public/api/studentcheckout?user_id='.$user_id.'&session_keys='.$session_key.'&request_id='.$request_id.'&amount='.$amount;
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/*-------- 1. student checkout url---------- */
/*--------2. student checkout ---------- */
public function studentcheckout()
{
	$input = Input::all();
	$user_id = isset($input['user_id'])?$input['user_id']:"";
	$session_keys = isset($input['session_keys'])?$input['session_keys']:"";
	$request_id = isset($input['request_id'])?$input['request_id']:"";
	$amount = isset($input['amount'])?$input['amount']:"";
	Session::put('id.session.id', $user_id);
	Session::put('id.session.session', $session_keys);
	return view('frontend.user.studentcheckout',compact('input'));
}
/*--------2. student checkout ---------- */
/*--------2. student checkout post---------- */
public function studentcheckoutpost()
{
	if(isset($_POST)){
		$input = Input::all();
		$user_id=isset($input['user_id'])?$input['user_id']:"";
		$request_id=isset($input['request_id'])?$input['request_id']:"";
		$tutor_id="";
		if(!empty($input['request_id'])){
			$tut_result=DB::table('assign_requests')->where('request_id',$input['request_id'])->first();
			if(count($tut_result)>0){
				$tutor_id=!empty($tut_result->tutor_id)?$tut_result->tutor_id:"";
			}
		}
		$detail ="card";
		$amount = isset($input['amount'])?$input['amount']:"";
		$name = isset($input['name'])?$input['name']:"";
		$email = isset($input['email'])?$input['email']:"";
		$phone = isset($input['phone'])?$input['phone']:"";
		$update_arr= array('cust_id'=>$user_id,'tutor_id'=>$tutor_id,'request_id'=>$request_id,'detail'=>$detail,'amount'=>$amount,'name'=>$name,'email'=>$email,'phone'=>$phone,'created_at'=>Date('Y-m-d H:i:s'),'updated_at'=>Date('Y-m-d H:i:s'));
		$orderid = DB::table('customer_order')->insertGetId($update_arr);	
		$input['orderid']=$orderid;
		$input['detail']=$detail;
		Session::put('id.session.orderid', $orderid);
	}
	else{
		$input = Input::all();
	}
	return view('frontend.user.studentcheckoutpost',compact('input'));
}
/*--------2. student checkout post---------- */


/* --------- fly current request -------------- */
public function flycurrentrequest(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) ){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
									}
								)
								->where('assign_requests.status', '=', 2)
								->where('assign_requests.tutor_id', '=', $user_id)
								->where('requests.booking_type', '=','fly')
								->get();
				//print_r($results);exit;
			if(count($results)>0){
				$resultsarray = array();
				$i=0;
				foreach($results as $rs){

					$resultsarray[$i]['user_id']= !empty($rs->user_id)?$rs->user_id:"";
					// show user detail 
					if(!empty($rs->user_id)){
						$user_detail_sc= DB::table('users')->where('id',$rs->user_id)->first();
						$resultsarray[$i]['user_name'] = !empty($user_detail_sc->name)?$user_detail_sc->name:"";
						$resultsarray[$i]['user_email'] = !empty($user_detail_sc->email)?$user_detail_sc->email:"";
						$resultsarray[$i]['user_pic'] = !empty($user_detail_sc->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail_sc->pic:"";
					}

					// show user detail 
					$resultsarray[$i]['request_id']= !empty($rs->request_id)?$rs->request_id:"";
					$resultsarray[$i]['request_date']= !empty($rs->request_date)?date('Y-m-d H:i:s',strtotime($rs->request_date)):"";
					$resultsarray[$i]['duration']= !empty($rs->duration)?$rs->duration:"";
					$resultsarray[$i]['location']= !empty($rs->location)?$rs->location:"";
					$resultsarray[$i]['latitude']= !empty($rs->latitude)?$rs->latitude:"";
					$resultsarray[$i]['longitude']= !empty($rs->longitude)?$rs->longitude:"";
					$subject_idarray = explode(',',$rs->subject_id);
					$subjects=array();
					foreach($subject_idarray as $k=>$subject){ 
						$resultsubject = DB::table('subject')->where('id',$subject)->first();
						if(count($resultsubject)>0){
							$subjects[$k]['subject_id']=$subject;
							$subjects[$k]['subject_name']=$resultsubject->title;
						}
							
					} 
					$resultsarray[$i]['subjects']=$subjects;
					$resultsarray[$i]['status']= !empty($rs->status)?$rs->status:"";
					++$i;
				}
				$resultArray['status']='1';
				$resultArray['message']='Success.';
				$resultArray['Data']=$resultsarray;
				return json_encode($resultArray);
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No fly request for tutor';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- fly current request -------------- */

/* --------- request status -------------- */
public function requeststatus(){ 
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$results = 	DB::table('assign_requests')
								->join('requests', function($join)
									{
										$join->on('assign_requests.request_id', '=', 'requests.id');
									}
								)
								->Where(function ($query) {
					                $query->where('assign_requests.status', '=', 1)
					                      ->orWhere('assign_requests.status', '=', 4);
					            })
								//->where('assign_requests.status', '=', 1)
								//->orWhere('assign_requests.status', '=', 4)
								->where('requests.user_id', '=', $user_id)
								->where('assign_requests.request_id', '=', $request_id)
								->first();
			if(count($results)>0){
				$resultsarray['tutor_id']= !empty($results->tutor_id)?$results->tutor_id:"";
				// show user detail 
				if(!empty($results->tutor_id)){
					$user_detail_sc= DB::table('users')->where('id',$results->tutor_id)->first();
					if(count($user_detail_sc)>0){
						$resultsarray['user_name'] = !empty($user_detail_sc->name)?$user_detail_sc->name:"";
						$resultsarray['user_email'] = !empty($user_detail_sc->email)?$user_detail_sc->email:"";
						$resultsarray['user_pic'] = !empty($user_detail_sc->pic)?"http://laravel.kart247.com/tutorondemand/public/images/users/".$user_detail_sc->pic:"";
					}
				}
				// show user detail 
				$resultsarray['request_id']= !empty($results->request_id)?$results->request_id:"";
				$resultsarray['request_date']= !empty($results->request_date)?date('Y-m-d H:i:s',strtotime($results->request_date)):"";
				$resultsarray['duration']= !empty($results->duration)?$results->duration:"";
				$resultsarray['location']= !empty($results->location)?$results->location:"";
				$resultsarray['latitude']= !empty($results->latitude)?$results->latitude:"";
				$resultsarray['longitude']= !empty($results->longitude)?$results->longitude:"";
				$subject_idarray = explode(',',$results->subject_id);
				$subjects=array();
				foreach($subject_idarray as $k=>$subject){ 
					$resultsubject = DB::table('subject')->where('id',$subject)->first();
					if(count($resultsubject)>0){
						$subjects[$k]['subject_id']=$subject;
						$subjects[$k]['subject_name']=$resultsubject->title;
					}
						
				} 
				$resultsarray['subjects']=$subjects;
				$resultsarray['status']= !empty($results->status)?$results->status:"";
				if(isset($results->status)){
					$msg="";
					if($results->status=1){
						$msg="request accepted";
						$arrivalstatus=DB::table('assign_requests')->where('user_id',$user_id)->where('request_id',$results->request_id)->first();
						if(count($arrivalstatus)>0){
							$resultsarray['arrived_status']=isset($arrivalstatus->arrived)?$arrivalstatus->arrived:"";
						}
					}
					elseif($results->status=4){
						$msg="request cancel by tutor";
					}
					$resultsarray['status_message']=$msg;
				}

				$resultArray['status']='1';
				$resultArray['message']='Success.';
				$resultArray['Data']=$resultsarray;
				return json_encode($resultArray);
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='No status';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}
/* --------- request status -------------- */


public function tutarrivalstatus(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$chk_request=DB::table('requests')->where('id',$request_id)->first();
			if(count($chk_request)>0){
				$assign_request=DB::table('assign_requests')->where('request_id',$request_id)->where('tutor_id',$user_id)->first();
				if(count($assign_request)>0){
					if($assign_request->status==0){
						$resultArray['status']='0';
						$resultArray['message']='Request is canceled by student.';
						return json_encode($resultArray);
					}
					else{
						$arrival_arr= array('arrived'=>1);
						DB::table('assign_requests')->where('request_id',$request_id)->where('tutor_id',$user_id)->update($arrival_arr);
						$resultArray['status']='1';
						$resultArray['message']='Tutor has arrived';
						return json_encode($resultArray);
					}	
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Request not assign to tutor';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid request';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}



public function studentstoptimer(){
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$user_id = !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";
	$request_id = !empty($_REQUEST['request_id']) ? $_REQUEST['request_id'] : "";
	if( (isset($session_key) && !empty($session_key)) && (isset($user_id) && !empty($user_id)) && (isset($request_id) && !empty($request_id))){
		$check_auth=$this->checkToken($access_token,$user_id, $session_key);
		if($check_auth['status']!=1){
			return json_encode($check_auth);
		}
		else{
			$chk_request=DB::table('requests')->where('id',$request_id)->first();
			if(count($chk_request)>0){
				$assign_request=DB::table('assign_requests')->where('request_id',$request_id)->where('user_id',$user_id)->first();
				if(count($assign_request)>0){
					$check_session=DB::table('request_sessions')->where('request_id',$request_id)->where('status',1)->first();
					if(count($check_session)>0){
						$resultArray['status']='1';
						$resultArray['message']='Request is completed.';
						$resultArray['Data']['session_id']=!empty($check_session->id)?$check_session->id:"";
						return json_encode($resultArray);
					}
					else{
						$resultArray['status']='0';
						$resultArray['message']='Request is under progress.';
						return json_encode($resultArray);
					}
				}
				else{
					$resultArray['status']='0';
					$resultArray['message']='Request not assign to tutor';
					return json_encode($resultArray);
				}
			}
			else{
				$resultArray['status']='0';
				$resultArray['message']='Invalid request';
				return json_encode($resultArray);
			}
		}
	}
	else {
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

}



