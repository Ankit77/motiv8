<?php namespace App\Http\Controllers\Frontend;

use DB,View,Session,Validator,Input,Redirect,Hash,Auth;
use Mail;
use App\Models\Access\User\User;
use App\Contactus;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Access\LoginRequest;
use App\Exceptions\GeneralException;
use Request;
use App\Challenges;
 
/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class ApiController extends Controller {

	/**
	 * @return \Illuminate\View\View
	 */
	
public function index()
{
	javascript()->put([
		'test' => 'it works!'
	]);
	return view('frontend.index');
}

public function checkToken($access_token,$user_id='',$session_key='')
{
	$token=env('ACCESS_TOKEN');
	if($access_token!=$token){
		$resultArray['status']='0';
		$resultArray['message']='Invalid token!';
		return $resultArray;
		die;
	}
	else{
		if($user_id!=''){
			if($session_key==''){
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$session_key=substr(str_shuffle($chars),0,8);
				$checkuser=DB::table('mobile_session')->where('user_id',$user_id)->first();
				if(count($checkuser)>0){
					$update_arr= array('session_key' => $session_key );
					DB::table('mobile_session')->where('id',$checkuser->id)->update($update_arr);
				}
				else{
					$update_arr= array('session_key' => $session_key ,'user_id'=>$user_id);
					DB::table('mobile_session')->insert($update_arr);
				}
				$resultArray['status']='1';
				$resultArray['Data']['randnumber']=$session_key;
				return ($resultArray);
			}
			else{
				$con_arr=array('user_id'=>$user_id,'session_key'=>$session_key);
				$checkuser=DB::table('mobile_session')->where($con_arr)->first();
				if(count($checkuser)>0){
					$resultArray['status']='1';
					$resultArray['Data']['randnumber']=$session_key;
					return ($resultArray); die;
					}
					else{
					$resultArray['status']='0';
					$resultArray['message']='Invalid session';
					return ($resultArray); die;
				}
			}
		}	
		else{
			$resultArray['status']='1';
			$resultArray['Data']['message']='';
			return ($resultArray); die;
		}	
	}
}
/* start api */

/***** 
Api Name:login api 
Paramenter: password or email
Output:login successfully api login array
***/

public function login()
{
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$pass = !empty($_REQUEST['password']) ? $_REQUEST['password'] : "";
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	if((isset($email) && !empty($email)) && (isset($pass) && !empty($pass))){
		$chkuserLogin = DB::table('users')->where('email',$email)->first();
		if(count($chkuserLogin)>0 && Hash::check($pass, $chkuserLogin->password)){
			if($chkuserLogin->confirmed==0){
				$resultArray['status']='0';
				$resultArray['message']='Your account is Inactive.';
				return json_encode($resultArray);
			}
			else{
				$check_auth=$this->checkToken($access_token,$chkuserLogin->id);
				if($check_auth['status']!=1)
				{
					return json_encode($check_auth);
				}
				else
				{
					$images="http://laravel.kart247.com/motiv8/public/images/users/".$chkuserLogin->profile_pic;
					!empty($chkuserLogin->set_theme)?
						 $theme=DB::table('themes')->where('id',$chkuserLogin->set_theme)->select('theme_color', 'theme_attribute')->first() : $theme->theme_color="";  

					$resultArray['status']='1';
					$resultArray['message']='Login Successfully';
					$resultArray['Data']['userprofile']['id']=!empty($chkuserLogin->id)?$chkuserLogin->id:"";
					$resultArray['Data']['userprofile']['name']=!empty($chkuserLogin->name)?$chkuserLogin->name:"";
					$resultArray['Data']['userprofile']['email']=!empty($chkuserLogin->email)?$chkuserLogin->email:"";
					$resultArray['Data']['userprofile']['profile_pic']=!empty($chkuserLogin->profile_pic)?$images:"";
					$resultArray['Data']['userprofile']['phone']=!empty($chkuserLogin->phone)?$chkuserLogin->phone:"";
					
						if(!empty($chkuserLogin->set_theme)){
							$resultArray['Data']['background']['type']=!empty($theme->theme_attribute)? $theme->theme_attribute :"" ;
						if($theme->theme_attribute="background"){
							$resultArray['Data']['background']['image']=!empty($theme->theme_color)? $theme->theme_color :"" ;
						}else{
							$resultArray['Data']['background']['color']=!empty($theme->theme_color)? $theme->theme_color :"" ;
						}
					}else
					{

					}

					$resultArray['Data']['Session_keys']=$check_auth['Data']['randnumber'];
					return json_encode($resultArray);
				}
			}
		}
		else{
			$resultArray['status']='0';
			$resultArray['message']='Invalid details.';
			return json_encode($resultArray);
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter';
		return json_encode($resultArray);
	}
}

/* login api end */

/***** 
Api Name:Register api 
Paramenter: name ,email ,phone ,image ,theme_id or password  
Output:Register successfully api login array
***/

public function register()
{
	$email = !empty($_REQUEST['email']) ? $_REQUEST['email'] : "";
	$password = !empty($_REQUEST['password']) ? $_REQUEST['password'] : "";
	$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : "";
	$phone = !empty($_REQUEST['phone']) ? $_REQUEST['phone'] : "";
	$theme_id = !empty($_REQUEST['theme_id']) ? $_REQUEST['theme_id'] : "";
	$image = !empty($_REQUEST['image']) ? $_REQUEST['image'] : "";
	if(isset($email) && !empty($email) && isset($password) && !empty($password) &&  isset($name) && !empty($name) &&  isset($phone) && !empty($phone)){
	
		$chkuserLogin = DB::table('users')->where('email',$email)->first();
		if(!empty($chkuserLogin) && (isset($chkuserLogin))){
				$resultArray['status']='0';
				$resultArray['message']='Email already exist.';
				return json_encode($resultArray);
		}
		else{
			$chkuserLoginPhone = DB::table('users')->where('phone',$phone)->first();
		if(!empty($chkuserLoginPhone) && (isset($chkuserLoginPhone))){
				$resultArray['status']='0';
				$resultArray['message']='Phone already exist.';
				return json_encode($resultArray);
		}else{
			if($image!="")
					{
						$setimag=$this->base64toimage($image);
						if($setimag['status']==1 && $setimag['imagename']!='')
						{
							$update_arr= array('name' => $name,
												'email' => $email,
												'phone' =>$phone,
												'password' => Hash::make($password),
												'confirmed' =>  1 ,
												'set_theme' => $theme_id,
												'pic' => $setimag['imagename']);
							DB::table('users')->insert($update_arr);
							
						}
					}else{
						$user = DB::table('users')->insert([
						'name' => $name,
						'email' => $email,
						'phone' =>$phone,
						'password' => Hash::make($password),
						'confirmed' =>  1 ,
						'set_theme' => $theme_id
					]);
					}
					
				
			$resultArray['status']='1';
			$resultArray['message']='Successfully registered.';
			return json_encode($resultArray);
		}
		}
	}
	else{
		$resultArray['status']='0';
		$resultArray['message']='Invalid parameter.';
		return json_encode($resultArray);
	}
	
	
}
/* end api */


/***** 
Api Name:theme listing get api 
Paramenter: user_id    
Output:theme listing successfully api  
***/

public function themes()
{

	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	
		$check_auth=$this->checkToken($access_token);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else 
		{			
			
				
				$settheme=DB::table('themes')->where('status', 1)->get();
				if(count($settheme)>0)
				{
					$i=0;
					foreach ($settheme as  $value) {
						$resultarray[$i]['theme_id']=$value->id;
						$resultarray[$i]['title']=$value->title;
						++$i;
					}




					$resultArray['status']='1';
					$resultArray['message']='theme successfully';
					$resultArray['Data']=$resultarray;
					return json_encode($resultArray);
				}else
				{
					$resultArray['status']='1';
					$resultArray['message']='Invalid theme';
					return json_encode($resultArray);
				}
			
		}
	
}
/* End api*/

/*
Api Name:  challenge listing for user 
Paramenter: user_id ,session_keys,  
Output:theme challenge successfully created 
*/
public function challengelisting()
{
	$access_token = Request::header('accesstoken');
	$access_token = !empty($access_token) ? $access_token : "";
	$session_key = !empty($_REQUEST['session_keys']) ? $_REQUEST['session_keys'] : "";

	$check_auth=$this->checkToken($access_token);
		if($check_auth['status']!=1)
		{
			return json_encode($check_auth);
		}
		else 
		{
			
			$challenge=DB::table('challenges')->where('status', 1)->get();
			$i=0;
			foreach ($challenge as  $value) {
				$result[$i]['name']=!empty($value->name)? $value->name :"";
				$result[$i]['description']=!empty($value->description)? $value->description :"" ;
				
				$userchallenge=DB::table('user_challenges')->where('challenge_id', $value->id )->get();
				foreach ($userchallenge as  $valueinfo) {
					$result[$i]['challenge_timeline']=!empty($valueinfo->challenge_timeline)? $valueinfo->challenge_timeline:"";
					$result[$i]['post_frequency']=!empty($valueinfo->post_frequency)? $valueinfo->post_frequency:"";
					$result[$i]['completion_reward']=!empty($valueinfo->completion_reward)? $valueinfo->completion_reward:"";
					$result[$i]['invite_friend']=!empty($valueinfo->invite_friend)? $valueinfo->invite_friend: "";
					$result[$i]['status']=!empty($valueinfo->status)? $valueinfo->status :"";


				}
			}


			$resultArray['status']='1';
			$resultArray['message']='successfully';
			$resultArray['data']=$result;
			return json_encode($resultArray);

		}	
	
}

public function base64toimage($base64)
{
	$data = base64_decode($base64); // base64 decoded image data
	$source_img = imagecreatefromstring($data);
	$rotated_img = imagerotate($source_img, 0, 10); // rotate with angle 90 here
	$fname= uniqid() . '.jpg';
	$file = 'images/users/'. $fname;
	$imageSave = imagejpeg($rotated_img, $file, 10);
	imagedestroy($source_img);
	
	if($fname)
	{
		$returndata['status']=1;
		$returndata['imagename']=$fname;
	}
	else{
		$returndata['status']=0;
		$returndata['imagename']='';
	}
	return $returndata;

}




}