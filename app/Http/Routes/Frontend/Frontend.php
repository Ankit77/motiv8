<?php

/**
 * Frontend Controllers
 */
Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('macros', 'FrontendController@macros')->name('frontend.macros');


Route::group(['prefix' => 'api'], function () {
		Route::post('login', ['as' => 'login', 'uses' => 'ApiController@login']);
		Route::post('register', ['as' => 'register', 'uses' => 'ApiController@register']);
		Route::post('theme', ['as' => 'theme', 'uses' => 'ApiController@themes']);
		Route::post('challengelisting', ['as' => 'challengelisting', 'uses' => 'ApiController@challengelisting']);
});	

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function() {
        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
    });
});


