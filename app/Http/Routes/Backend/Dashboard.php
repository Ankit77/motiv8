<?php

Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
Route::get('themes', 'DashboardController@themes')->name('admin.themes');
Route::get('addtheme', 'DashboardController@addtheme')->name('admin.addthemepage');
Route::post('addtheme', 'DashboardController@savetheme')->name('admin.addtheme');
Route::get('deletetheme/{id}', 'DashboardController@deletetheme');
Route::get('deactivetheme/{id}', 'DashboardController@deactivetheme');
Route::get('activetheme/{id}', 'DashboardController@activetheme');
Route::get('viewuser/{id}', 'DashboardController@viewuser')->name('admin.viewuser');
Route::get('edittheme/{id}', 'DashboardController@edittheme')->name('admin.edittheme');
Route::post('edittheme', 'DashboardController@editthemesave')->name('admin.editthemesave');
Route::get('viewtheme/{id}', 'DashboardController@viewtheme')->name('admin.viewtheme');
Route::get('challenges', 'DashboardController@challenges' )->name('admin.challenges');
Route::get('addchallenges', 'DashboardController@addchallenges' )->name('admin.addchallenges');
Route::post('addchallenges', 'DashboardController@savechallenges' )->name('admin.savechallenges');
Route::get('editchallenge/{id}', 'DashboardController@editchallenge' )->name('admin.editchallenge');
Route::post('editchallenge', 'DashboardController@editchallengesave' )->name('admin.editchallengesave');
Route::get('deactivechallenge/{id}', 'DashboardController@deactivechallenge');
Route::get('activechallenge/{id}', 'DashboardController@activechallenge');
Route::get('deletechallenge/{id}', 'DashboardController@deletechallenge');
Route::get('viewchallenge/{id}', 'DashboardController@viewchallenge')->name('backend.viewchallenge');

