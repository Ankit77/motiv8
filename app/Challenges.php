<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Contactus
 * @package App
 */
class Challenges extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'challenges';

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
}
