@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Challenge Management" }}
        <small>{{ 'View Challenge' }}</small>
    </h1>
@endsection

@section('content')
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> View Challenge</h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
               	<a href="{{ url('admin/addchallenge')}}" class="btn btn-primary">Add Challenge</a>
               	
               </div>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover">
                <thead>

                @foreach($challenges as $challenge)
                <tr>
                    <th> Name</th>
                    <th>{{ $challenge->name }}</th>
                </tr>
                <tr>
                    <th>Challenge Description</th>
                    <th>$challenge->description</th>
                </tr>
                <tr>
                    <th>Challenge Timeline</th>
                    <th>$challenge->challenge_timeline</th>
                </tr>
                <tr>
                    <th>Post Frequency</th>
                    <th>$challenge->post_frequency</th>
                </tr>
                <tr>
                    <th>Completion Reward </th>
                    <th>$challenge->completion_reward</th>
                </tr>
                    <tr>
                    <th>Invite Friend </th>
                    <th>$challenge->invite_friend</th>
                </tr>


                @endforeach
                </thead>
                </table>
              </div>
          </div>
          
           

</div>
 
@stop
