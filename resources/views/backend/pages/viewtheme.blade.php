@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Themes Management" }}
        <small>{{ 'View theme' }}</small>
    </h1>
@endsection

@section('content')
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> View Themes</h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
               	<a href="{{ url('admin/addtheme')}}" class="btn btn-primary">Add theme</a>
               	
               </div>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover">
                <thead>

                @foreach($themes as $theme)
                <tr>
                <th> Title</th>
                <th>{{ $theme->title }}</th>
                </tr>
                @if($theme->theme_attribute=="color")
                <tr>
                <th>Background Color</th>
                <th><div class="col-md-3">
                    <div class="col-md-12"  style="background-color:{{ $theme->theme_color }}; height:100px; width:100px; "></div>
                        </div>
                        
                    </th>
                </tr>
                @else
                <tr>
                <th>Background Image</th>
                <th><div class="col-md-3">
                             <img height="100" src="{{ asset('images/background/'.$theme->theme_color) }}" onerror="this.src='{{ asset('images/no-user.jpg') }}';" data-src="default.jpg"  data-holder-rendered="true" />
         
                        </div>
                        
                    </th>
                </tr>
                @endif
                @endforeach
                </thead>
                </table>
              </div>
          </div>
          
           

</div>
 
@stop
