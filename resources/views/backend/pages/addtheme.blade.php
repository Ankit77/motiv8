@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Themes Management" }}
        <small>{{ 'Create themes' }}</small>
    </h1>
@endsection

@section('content')
<style type="text/css">
  #backgeound
  {
    display: none;
  }
</style>
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> Add Themes</h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
               	<a href="{{ url('admin/addtheme')}}" class="btn btn-primary">Add theme</a>
               	
               </div>
            </div>
        </div><!-- /.box-header -->

      <div class="box-body">
            <div class="table-responsive">
          {!! Form::open(['route' => 'admin.addtheme', 'id' =>'myform','class' => 'form-horizontal','method' => 'POST',  'files'=>true ]) !!}
            <div class="col-md-12">
         

  
                      <div class="form-group">
                            {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::input('text', 'title', '', ['class' => 'validate[required] form-control', 'placeholder' => ' Enter title']) !!}
                            </div><!--col-md-6-->
                        </div><!--form-group-->
                      <div class="form-group">
                            {!! Form::label('attribute', 'attribute', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                            Color:
                           
                           {!! Form::radio('theme_attribute','color',true,['class'=>'validate[required]', 'id'=>'setcolor'])!!}
                           Background:
                           {!!   Form::radio('theme_attribute', 'background',false,['class'=>'validate[required]', 'id'=>'setbackground']) !!}
                             </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group" id="color">
                           {!! Form::label('theme_color', 'Color', ['class' => 'col-md-4 control-label']) !!}
                          
                            <div class="col-md-6">
                                {!! Form::input('textbox', 'theme_color', '', ['class' => 'jscolor form-control ' ,'id'=>'theme']) !!}
                            </div><!--col-md-6-->
                        </div>
                       <div class="form-group" id="backgeound">
                            {!! Form::label('theme_color', 'Background', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                               <input type="file" name="theme_pic"  class=" form-control" id="user_profile"   />
                            </div><!--col-md-6-->
                        </div>
                
                
            <div class="form-group">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <input type="submit" class="btn btn-primary " value ="save">
             </div>
             </div>
        
              
         </div>
            {!! Form::close() !!}
              </div>
          </div>
          
            <div class="pull-right">
                
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
 <SCRIPT LANGUAGE="JavaScript">

         $('#setcolor').on('change', function(){
            if($(this).prop("checked") == true){
              $('#color').show(); 
             $('#backgeound').hide();
             
            }

         });
          $('#setbackground').on('change', function(){
            if($(this).prop("checked") == true){
              $('#backgeound').show();
                $('#color').hide(); 
          
            }

         });
 
    </SCRIPT>
@stop
