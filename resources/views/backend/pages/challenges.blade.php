@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Challenges Management" }}
        <small>{{ 'Challenges Listing' }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> </h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
                <a href="{{ url('admin/addchallenges')}}" class="btn btn-primary">Add challenges</a>
                
               </div>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
          
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Challenge timeline</th>
                    <th>Post frequency</th>
                    <th>Status</th>
                    <th>Completion reward</th>
                    <th></th>
                </tr>
                </thead>
                @if(isset($challenges))
                <?php $i=1; ?>
                @foreach($challenges as $challenge)
                 <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $challenge->name }}</td>
                    <td>{{ $challenge->description }}</td>
                    <td>{{ $challenge->challenge_timeline }}</td>
                    <td> {{ $challenge->post_frequency }}</td>

                    <td>{{ $challenge->status }}</td>
                    <td>{{ $challenge->completion_reward }}</td>
                    <td>
                                <a class="btn btn-xs btn-primary" href="{{ url('admin/editchallenge/'.$challenge->id )}}"><i title="" data-placement="top" data-toggle="tooltip" class="fa fa-pencil" data-original-title="Edit"></i></a>
                        @if($challenge->status==1)
                                <a href="{{ url('admin/deactivechallenge/'.$challenge->id )}}" class="btn btn-xs btn-warning"><i data-original-title="Deactivate" class="fa fa-pause" data-toggle="tooltip" data-placement="top" title=""></i></a> 
                            @else
                                <a href="{{ url('admin/activechallenge/'.$challenge->id )}}" class="btn btn-xs btn-warning"><i data-original-title="activate" class="fa fa-play" data-toggle="tooltip" data-placement="top" title=""></i></a> 
                            @endif
                            <a   class="btn btn-xs btn-danger" href="{{ url('admin/deletechallenge/'.$challenge->id )}}"><i data-original-title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title=""></i> </a>
                           <a class="btn btn-xs btn-primary" href="{{ url('admin/viewchallenge/'.$challenge->id) }}"><i title="" data-placement="top" data-toggle="tooltip" class="fa fa-eye" data-original-title="View"></i></a>
                    </td>
                </tr>
                <?php $i++; ?>
                @endforeach
                @endif
            </table>

              </div>
          </div>
          
            <div class="pull-right">
                
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
       