@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Themes Management" }}
        <small>{{ 'Themes Listing' }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> Active Themes</h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
               	<a href="{{ url('admin/addtheme')}}" class="btn btn-primary">Add theme</a>
               	
               </div>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
          
            <table class="table table-striped table-bordered table-hover">
                <thead>
	            <tr>
	            	<th>id</th>
	            	<th>title</th>
	            	
                
	            	<th>status</th>
	            	<th></th>
	        	</tr>
	        	</thead>
	        	@if(isset($themes))
	        	<?php $i=1; ?>
	        	@foreach($themes as $theme)
	        	 <tr>
	            	<td>{{ $i }}</td>
	            	<td>{{ $theme->title }}</td>
	            	
	            	<td>{{ $theme->status }}</td>
	            	<td>
	            				<a class="btn btn-xs btn-primary" href="{{ url('admin/edittheme/'.$theme->id )}}"><i title="" data-placement="top" data-toggle="tooltip" class="fa fa-pencil" data-original-title="Edit"></i></a>
	            		@if($theme->status==1)
	            				<a href="{{ url('admin/deactivetheme/'.$theme->id )}}" class="btn btn-xs btn-warning"><i data-original-title="Deactivate" class="fa fa-pause" data-toggle="tooltip" data-placement="top" title=""></i></a> 
	            			@else
	            				<a href="{{ url('admin/activetheme/'.$theme->id )}}" class="btn btn-xs btn-warning"><i data-original-title="activate" class="fa fa-play" data-toggle="tooltip" data-placement="top" title=""></i></a> 
	            			@endif
	            			<a   class="btn btn-xs btn-danger" href="{{ url('admin/deletetheme/'.$theme->id )}}"><i data-original-title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title=""></i>
								</a>
								<a class="btn btn-xs btn-primary" href="{{ url('admin/viewtheme/'.$theme->id) }}"><i title="" data-placement="top" data-toggle="tooltip" class="fa fa-eye" data-original-title="View"></i></a>
					</td>
	        	</tr>
	        	<?php $i++; ?>
	        	@endforeach
	        	@endif
            </table>

              </div>
          </div>
          
            <div class="pull-right">
                
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
