@extends('backend.layouts.master')

@section('page-header')
    <h1>
      {{ trans('Users Informations') }}

   
    </h1>
@endsection

@section('breadcrumbs')
    <li><a href="{!! url('admin/access/users') !!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
   <!--  <li><a href="{!! url('admin/access/aditional_info')!!}/"> {{ trans('Aditional Information') }}</a></li>
   -->  <li class="active">{{ trans('View Profile') }}</li>
@endsection

@section('content')


@foreach($userdetail as $user)

 <div class="col-md-10">
          <div class="pro-imguser">         
          <img height="100" src="{{ asset('images/users/'.$user->profile_pic) }}" onerror="this.src='{{ asset('images/no-user.jpg') }}';" data-src="default.jpg"  data-holder-rendered="true" />
          </div>
          <div class="pro-text">
          <h4> Name: @if($user->name==""){{  "" }} @else {{ $user->name }}@endif</h4>
         <table cellspacing="0" class="data_table">
          <tbody>
          
          <tr>
            <td>Phone:</td>
            <td><h4>{!! !empty($user->phone)? $user->phone :""; !!} </h4>   </td>
          </tr>
        
       
          
          <tr>
          <th><h4>Joined On:</h4></th>
          <td><h4>
            {!! !empty($user->created_at)? date('F d, Y', strtotime($user->created_at)) :""; !!}
          </h4></td>
          </tr>  
        </tbody></table>
       </div>
      </div>

      @endforeach

  

@endsection