@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ "Challenges Management" }}
        <small>{{ 'Create challenge' }}</small>
    </h1>
@endsection

@section('content')
 <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> </h3>

            <div class="box-tools pull-right">
               <div class="pull-right">
               	<a href="{{ url('admin/challenges')}}" class="btn btn-primary">Add challenges</a>
               	
               </div>
            </div>
        </div><!-- /.box-header -->

      <div class="box-body">
            <div class="table-responsive">
          {!! Form::open(['route' => 'admin.savechallenges', 'id' =>'myform','class' => 'form-horizontal']) !!}
            <div class="col-md-12">
         
                <div class="form-group">
                      {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'name', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->
                <div class="form-group">
                      {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'description', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->
                <div class="form-group">
                      {!! Form::label('challenge_timeline', 'Challenge Timeline', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'challenge_timeline', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->
                <div class="form-group">
                      {!! Form::label('post_frequency', 'Post Frequency', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'post_frequency', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->
               <div class="form-group">
                      {!! Form::label('completion_reward', 'Completion Reward ', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'completion_reward', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->
              <div class="form-group">
                      {!! Form::label('invite_friend', 'Invite Friend', ['class' => 'col-md-4 control-label']) !!}
                      <div class="col-md-6">
                          {!! Form::input('text', 'invite_friend', '', ['class' => 'validate[required] form-control']) !!}
                      </div><!--col-md-6-->
                  </div><!--form-group-->

                  
          
                
            <div class="form-group">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <input type="submit" class="btn btn-primary " value ="save">
             </div>
             </div>
        
              
         </div>
            {!! Form::close() !!}
              </div>
          </div>
          
            <div class="pull-right">
                
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
 
@stop
