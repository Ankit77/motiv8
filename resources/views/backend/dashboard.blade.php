@extends('backend.layouts.master')

@section('page-header')
    <h1>
        {!! app_name() !!}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {!! access()->user()->name !!}!</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php /* ?>{!! getLanguageBlock('backend.lang.welcome') !!} <?php */ ?>
			<!-- .row start -->
			<div class="col-md-8">
				<!-- col-md-8 start here -->
				<div class="row">
					<!-- .row start -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-6 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number dolar" data-from="0" data-to="568">8</span>
									<span class="stats-icon">
							<i class="fa fa-money color-green"></i>
						</span>
									<h5>Languages</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-6 end here -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-6 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number" data-from="0" data-to="317">2</span>
									<span class="stats-icon">
							<i class="fa fa-shopping-cart color-yellow-dark"></i>
						</span>
									<h5>Users</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-6 end here -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-4 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number" data-from="0" data-to="17">0</span>
									<span class="stats-icon">
							<i class="fa fa-comments color-blue"></i>
						</span>
									<h5>Rooms</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-4 end here -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-4 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number" data-from="0" data-to="48">10</span>
									<span class="stats-icon">
							<i class="fa fa-support color-red"></i>
						</span>
									<h5>Support requests</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-4 end here -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-4 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number" data-from="0" data-to="5">0</span>
									<span class="stats-icon">
							<i class="fa fa-calendar color-gray"></i>
						</span>
									<h5>New events</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-4 end here -->
					<div class="col-lg-4 col-md-6 col-xs-6 col-small-enlarge">
						<!-- col-md-4 start here -->
						<div class="panel panel-default">
							<!-- Start .panel -->
							<div class="panel-body">
								<a class="lead-stats" href="#">
									<span class="stats-number" data-from="0" data-to="3">3</span>
									<span class="stats-icon">
							<i class="fa fa-envelope color-green-light"></i>
						</span>
									<h5>Unread emails</h5>
								</a>
							</div>
						</div>
						<!-- End .panel -->
					</div>
					<!-- col-md-4 end here -->
				</div>
				<!-- / .row -->
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection